
# >pylint tuquex.py

# --------------------------------------------------------------------
# Your code has been rated at 10.00/10

"""
Alice, Bob, "" (3 words)

This code Given a name, return a string
Two-fer or 2-fer is short for two for one. One for you and one for me.

"""

from __future__ import print_function


def two_fer(name):

    """
    This function evaluates the condition for variable name.
    """

    if name == (''):
        print("One For You, One For me")
    else:
        print("One For "+name+", One For me")


for words in open("DATA.lst", "r"):
    characters = words.rstrip('\n')
    two_fer(characters)

# > python ./tuquex.py
# One For Alice, One For me
# One For Bob, One For me
# One For You, One For me
