# > pylint kzccardona.py
#
#    -------------------------------------------------------------------
#    Your code has been rated at 10.00/10

"""
This code Search a file for lines matching a regular expression pattern.
Return the line number and contents of each matching line.
patterns or commands:
-n Print the line numbers of each matching line.
-l Print only the names of files that contain at least one matching line.
-i Match line using a case-insensitive comparison.
-v Invert the program -- collect all lines that fail to match the pattern.
-x Only match entire lines, instead of lines that contain a match.
"""
from __future__ import print_function


def grep(pattern, flags, files):

    """
    This function extracts the data and applies the filters
    """
    matches = ""
    flag_list = ['-n', '-l', '-i', '-x', '-v']
    flags = flags.split(' ')
    if flag_list[2] in flags:
        pattern = pattern.lower()

    for file_data in files:
        text = open(file_data, 'r')
        text = text.readlines()

        for index, line in enumerate(text):

            if flag_list[2] in flags:
                line2 = line.lower()
            else:
                line2 = line

            if flag_list[3] in flags:
                match_line = pattern == line2.strip()
            else:
                match_line = pattern in line2
            if flag_list[4] in flags:
                match_line = not match_line

            if match_line:
                if flag_list[1] in flags:
                    matches += file_data + "\n"
                    break

                if len(files) > 1:
                    matches += file_data + ":"

                if flag_list[0] in flags:
                    matches += str(index + 1) + ":"
                matches += line
    return matches


FILES = ["DATA.lst"]
print(grep("Of", "-v", FILES))


# > python ./kzccardona.py
# Brought Death into the World, and all our woe,
# With loss of Eden, till one greater Man
# Restore us, and regain the blissful Seat,
# Sing Heav'nly Muse, that on the secret top
# That Shepherd, who first taught the chosen Seed
