"""
pylint tuquex.py

--------------------------------------------------------------------
Your code has been rated at 10.00/10

cabbage, scrabble, word (3 words)

Given a word, compute the scrabble score for that word
according to the following table letter Values

"""
from __future__ import print_function


def score(word):

    """
    This Function determines the punctuation for the word entered
    in the game scrabble
    """
    word = word.lower()
    one = ['a', 'e', 'i', 'o', 'u', 'l', 'n', 'r', 's', 't']
    two = ['d', 'g']
    three = ['b', 'c', 'm', 'p']
    four = ['f', 'h', 'v', 'w', 'y']
    five = ['k']
    eight = ['j', 'x']
    ten = ['q', 'z']
    points = 0

    for i, __ in enumerate(word):
        if word[i] in one:
            points += 1
        elif word[i] in two:
            points += 2
        elif word[i] in three:
            points += 3
        elif word[i] in four:
            points += 4
        elif word[i] in five:
            points += 5
        elif word[i] in eight:
            points += 8
        elif word[i] in ten:
            points += 10
    print(points)


for words in open("DATA.lst", "r"):
    characters = words.rstrip('\n')
    score(characters)


# > python ./tuquex.py
# 14
# 14
# 8
