# > pylint kzccardona.py
#
#    -------------------------------------------------------------------
#    Your code has been rated at 10.00/10

"""
This code Convert a number, represented as a sequence of digits
In one base, to any other base.
"""
from __future__ import print_function


class AnyBase(object):

    """
        This class Convert a number, represented as a sequence
        Of digits in one base, to any other base.
    """
    def __init__(self, error_code, error_messages, value):

        """
        This function initialize errors
        """
        self.error_code = error_code
        self.error_messages = error_messages
        self.value = value

    @classmethod
    def rebase(cls, input_base, digits, output_base):

        """
        This function proccess the data
        """
        decimal_converted = []
        converted = ""
        converted_output = []
        if BASE.verifiy_data(input_base, digits, output_base):
            decimal_converted = BASE.decimal(input_base, digits)
            for digit in decimal_converted:
                converted += str(digit)
                converted_output = (BASE.quotients(int(output_base),
                                                   int(converted)))
            return converted_output

        else:
            raise ValueError(ERROR_MESSAGES[ERROR_CODE])

    @classmethod
    def quotients(cls, base, digits):

        """
        This function extract the quotients of the number in
        Especific base
        """
        quotient = []
        module = 0
        while digits >= base:
            module = digits % base
            digits = digits // base
            quotient.append(int(module))

        quotient.append(digits)
        quotient.reverse()
        return quotient

    @classmethod
    def decimal(cls, base, digits):

        """
        This function convert the number to decimal
        """
        count = len(digits)-1
        converted_number = 0
        number_list = []
        for digit in digits:
            converted_number += (digit * base ** count)
            count -= 1

        if converted_number > 10:
            for digit in str(converted_number):
                number_list.append(int(digit))
        elif converted_number > 0 and converted_number < 10:
            number_list.append(converted_number)

        return number_list

    @classmethod
    def verifiy_data(cls, input_bs, digits, output):

        """
        This function verify if data have negative numbers
        """
        flag = False
        if input_bs < 2:
            BASE.error_code = 1
            BASE.value = False
        elif output < 2:
            BASE.error_code = 2
            BASE.value = False
        elif sum(1 for digit in digits if digit < 0) > 0:
            BASE.error_code = 3
            BASE.value = False
        elif all(numbers < input_bs for numbers in digits) == flag:
            BASE.error_code = 4
            BASE.value = False
        return VALUE


VALUE = True
ERROR_CODE = 0
ERROR_MESSAGES = {1: "Input base should be greater than 1",
                  2: "Output base should be greater than 1",
                  3: "Digits can't contains negative numbers",
                  4: "Digits can't be greater than input base"}
BASE = ()
BASE = AnyBase(ERROR_CODE, ERROR_MESSAGES, VALUE)

for row in open('DATA.lst', 'r'):
    row = row.rstrip()
    numberlist = row.split(';')
    base_inp = int(numberlist[0])
    base_out = int(numberlist[2])
    digit_list = str(numberlist[1]).replace(' ', '')
    digit_list = list(digit_list.split(','))
    digit_list = list(map(int, digit_list))
    print(BASE.rebase(base_inp, digit_list, base_out))


# > python ./kzccardona.py
# [4, 2]
# [1, 0, 1, 0, 1, 0]
# [1, 1, 2, 0]
