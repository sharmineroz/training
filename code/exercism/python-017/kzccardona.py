# > pylint kzccardona.py
#
#    -------------------------------------------------------------------
#    Your code has been rated at 10.00/10

"""
This code Tally the results of a small football competition.
"""
from __future__ import print_function


def tally(rows):

    # type: (list[str]) -> str
    """
    This function process the results of a small football competition.
    MP: Matches Played
    W: Matches Won = 3 points
    D: Matches Drawn (Tied) = 1 point
    L: Matches Lost = 0 points
    P: Points
    """
    unique_list = []
    score_matrix = {}
    type(score_matrix)

    for rowlen in rows:
        rows[rows.index(rowlen)] = rows[rows.index(rowlen)].split(';')

    for row in rows:
        for col in range(0, len(rows[0])-1):
            if row[col] not in unique_list:
                unique_list.append(row[col])

    for team in unique_list:
        score_matrix[team] = {'Team': team, 'MP': 0, 'W': 0,
                              'D': 0, 'L': 0, 'P': 0}

    for row in rows:
        if row[2] == "win":
            score_matrix[row[0]]['MP'] += 1
            score_matrix[row[0]]['W'] += 1
            score_matrix[row[0]]['P'] += 3

            score_matrix[row[1]]['MP'] += 1
            score_matrix[row[1]]['L'] += 1

        elif row[2] == "draw":
            score_matrix[row[0]]['MP'] += 1
            score_matrix[row[0]]['D'] += 1
            score_matrix[row[0]]['P'] += 1

            score_matrix[row[1]]['MP'] += 1
            score_matrix[row[1]]['D'] += 1
            score_matrix[row[1]]['P'] += 1

        elif row[2] == "loss":
            score_matrix[row[0]]['MP'] += 1
            score_matrix[row[0]]['L'] += 1

            score_matrix[row[1]]['MP'] += 1
            score_matrix[row[1]]['W'] += 1
            score_matrix[row[1]]['P'] += 3

    score_string = ('{: <26}'.format("Team") + "| MP |  W |  D |  L |  P |\n")
    for keys, value in sorted(score_matrix.items(), key=lambda i: i[0][0]):
        score_string += ('{: <26}'.format(keys) + "|  " +
                         str(value['MP']) + " |  " +
                         str(value['W']) + " |  " +
                         str(value['D']) + " |  " +
                         str(value['L']) + " |  " +
                         str(value['P']) + " |\n")
    return score_string


READ_DATA = open('DATA.lst', 'r').read()
READ_DATA = "".join(READ_DATA.splitlines())
RESULTS = READ_DATA.split(',')
print(tally(RESULTS))


# > python ./kzccardona.py
# Team                      | MP |  W |  D |  L |  P |
# Allegoric Alaskans        |  3 |  2 |  1 |  0 |  7 |
# Blithering Badgers        |  3 |  0 |  1 |  2 |  1 |
# Courageous Californians   |  3 |  2 |  1 |  0 |  7 |
# Devastating Donkeys       |  3 |  0 |  1 |  2 |  1 |
