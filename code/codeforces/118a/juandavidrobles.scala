object JuanDavidRobles {
  def main(args: Array[String]): Unit = {

    import scala.io.StdIn

    var line : String = StdIn.readLine()

    var string : String = ""

    var char : Char = 'a'

    for (i <- 0 until line.length){
      char = line.charAt(i)
      if (!char.equals('a') && !char.equals('e') &&
        !char.equals('i') && !char.equals('o') &&
        !char.equals('u') && !char.equals('y') &&
        !char.equals('A') && !char.equals('E') &&
        !char.equals('I') && !char.equals('O') &&
        !char.equals('U') && !char.equals('Y')){

        if (char.toInt >= 65 && char <= 90){
          char = (char.toInt+32).toChar
        }
        string = string + "." + char.toString
      }
    }
    println(string)
  }
}
