'''
Felipe@Felipe-PC MINGW64 /d/fluid/training/code/codeforces/1139a (fgomezoso)
$ pylint fgomezoso.py

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''
from __future__ import print_function


def is_even(num):

    '''returns true when the number is even'''
    return bool(num % 2 == 0)


def main():

    '''
    *This program prints the number of even substrings in a string "s"

    If the number at position i is even, then all substrings from this
    position are even. So string[i] has i+1 "even" substrings
    '''

    data = open("DATA.lst", "r")

    str_length = int(data.readline().rstrip('\n'))
    string = data.readline().rstrip('\n')
    count_even = 0

    for i in range(str_length):

        int_str = int(string[i])

        if is_even(int_str):
            count_even = count_even + i + 1

    print(count_even)

    data.close()


main()


# Felipe@Felipe-PC MINGW64 /d/fluid/training/code/codeforces/1139a (fgomezoso)
# $ python fgomezoso.py
# 327
