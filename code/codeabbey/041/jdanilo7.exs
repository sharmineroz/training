"""
$ mix credo --strict jdanilo7.exs
Checking 1 source file ...

Please report incorrect results: https://github.com/rrrene/credo/issues

Analysis took 0.9 seconds (0.2s to load, 0.6s running checks)
9 mods/funs, found no issues.

$ mix compile jdanilo7.exs #compilation
"""

defmodule Median do
  def median(a, b, c) do
    if a < b do
      helper(a, b, c)
    else
      helper(b, a, c)
    end
  end

  def helper(a, b, c) do
    if b < c do
      b
    else
      if c < a do
        a
      else
        c
      end
    end
  end

  def process_list([head | tail]) do
    Integer.to_string(median(hd(head), hd(tl(head)), hd(tl(tl(head))))) <>
    " " <> process_list(tail)
  end

  def process_list([]) do
    ""
  end

  def make_int([head | tail]) do
    [String.to_integer(head) | make_int(tail)]
  end

  def make_int([]) do
    []
  end

  def split_lines([head | tail]) do
    [make_int(String.split(head, " ")) | split_lines(tail)]
  end

  def split_lines([]) do
    []
  end

end

data = File.read("DATA.lst")
input = String.split(elem(data, 1), "\r\n")
input = Enum.drop input, -1
input = Median.split_lines(tl(input))
IO.puts (Median.process_list(input))

"""
$elixir jdanilo7.exs
4 1087 22 57 503 80 161 811 620 112 952 52 15 847 13 90 38 36 308 14 971 117
250 284 11 929 151 358
"""
