#=
 $julia
 julia> using Lint
 julia> lintfile("jdiego10.jl")
=#

open("DATA.lst") do file
  flag = false
  for ln in eachline(file)
     a=0
     b=0
     c=0
     if flag == false
         flag = true
         continue
       end
     a=parse(Int64)
     b=parse(Int64)
     c=parse(Int64)
     if (a<b && a>c) || (a>b && a<c)
         printl(a)
      end
     if (b<a && b>c) || (b>a && b<c)
         printl(b)
      end
     if (c<b && c>a) || (c>b && c<a)
         printl(c)
      end
   end
end

#=
 $julia jdiego10.jl
 4
1087
22
57
503
80
161
811
620
112
952
52
15
847
13
90
38
36
308
14
971
117
250
284
11
929
151
358
=#
