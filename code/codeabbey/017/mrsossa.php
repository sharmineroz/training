<?php
/*
$ php -l mrsossa.php
No syntax errors detected in mrsossa.php
*/

$fp = fopen("DATA.lst", "r");
$sum = -1;
while (!feof($fp)){
  if($sum==-1){
    $sum=0;
  }else{
    $line = fgets($fp);
    $token = strtok($line, " \n\t");
    while($token !== false) {
      $sum += $token;
      $sum *= 113;
      if($sum>10000007){
          $sum %= 10000007;
      }
      $token = strtok(" \n\t");
    }
  }
}
echo $sum;

/*
$ php mrsossa.php
2410846
*/
?>

