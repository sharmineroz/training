'''
Problem #80 Duel Chances
$ pylint develalopez.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 develalopez.py
'''

from __future__ import print_function


def alan_wins(alan, bob):
    '''
    Function that returns the probability that Alan wins the duel.
    '''
    alan /= 100
    bob /= 100
    intersection = alan + bob - (alan * bob)
    victory = alan / intersection
    return round(victory * 100)


def main():
    '''
    Main fucntion that reads input and prints the solution.
    '''
    d_file = open("DATA.lst", "r")
    cases = int(d_file.readline())
    answers = []
    for _ in range(cases):
        alan, bob = map(int, [int(x) for x in d_file.readline().split()])
        answers.append(alan_wins(alan, bob))
    print(" ".join(str(x) for x in answers))


main()

# pylint: disable=pointless-string-statement

'''
$ py develalopez.py
48 46 94 72 89 72 71 73 58 50 55 52 87 85 76 96 66 88 35 93 82 61 74 40
'''
