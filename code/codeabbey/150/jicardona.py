#!/usr/bin/env python2.7
'''
$ pylint jicardona.py
No config file found, using default configuration

------------------------------------
Your code has been rated at 10.00/10
'''

import re

TEST_CASES = []
SOLUTION = []

with open('DATA.lst') as data:
    for line in data:
        repl = line.strip()
        repl = re.sub(r'(?i)(^0X(\d|[A-F])+$|^\d(\d|[A-F])*H$)', 'hex', repl)
        repl = re.sub(r'^(?!0)\d+$', 'dec', repl)
        repl = re.sub(r'^0[0-7]+$', 'oct', repl)
        repl = re.sub(r'(?i)(^0B[0-1]+$|^[0-1]+B$)', 'bin', repl)
        SOLUTION.append(repl)

print ' '.join(map(str, SOLUTION))

# pylint: disable=pointless-string-statement

'''
$ python jicardona.py
oct        dec dec bin 088   dec J2DH dec hex hex oct         bin
0B21111210 bin hex hex oct   oct dec  dec dec bin 0b110202001 031a1
dec        oct hex hex 0xghf oct oct  bin oct bin bin         hex
'''
