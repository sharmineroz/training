/*
  $ dartanalyzer adrianfcn.dart
Analyzing adrianfcn.dart...
No issues found!
*/
import 'dart:math' as math;
import 'dart:io';
import "dart:convert";

void swap(List arr, int a, int b) {
  var box = List(3);
  box[0] = arr[a][0];
  box[1] = arr[a][1];
  box[2] = arr[a][2];
  arr[a][0] = arr[b][0];
  arr[a][1] = arr[b][1];
  arr[a][2] = arr[b][2];
  arr[b][0] = box[0];
  arr[b][1] = box[1];
  arr[b][2] = box[2];
}

void sortingList(List l) {
  for (int i = 0; i < l.length; i++) {
    for (int j = 0; j < l.length; j++) {
      if (l[i][1] < l[j][1]) {
        swap(l, i, j);
      } else if (l[j][1] == l[i][1]) {
        if (l[i][0] < l[j][0]) {
          swap(l, i, j);
        }
      }
    }
  }
}

List rotate(int x, int y, double angle, String name) {
  List r = List();
  r.add((x*math.cos(angle) - y*math.sin(angle)).round());
  r.add((x*math.sin(angle) + y*math.cos(angle)).round());
  r.add(name);
  return r;
}

void main() {
  final List<String> source = File("DATA.lst")
    .readAsStringSync(encoding: utf8)
    .split("\n");
  var inputs = source.map((s) => (s.split(" "))).toList();
  List output = List();
  String sol = "";
  double angle = int.parse(inputs[0][1]) * (math.pi / 180);
  for (int i = 1; i < inputs.length - 1; i ++) {
    output.add(rotate(int.parse(inputs[i][1]),
                  int.parse(inputs[i][2]),
                  angle,
                  inputs[i][0]));
  }
  sortingList(output);
  for (var i in output) {
    sol += i[2] + " ";
  }
  print(sol);
}
/*
  $ dart adrianfcn
Algol Deneb Thabit Aldebaran Mira Sirius Kastra Heka Electra Capella Gemma
Rigel Mizar Albireo Yildun Polaris Unuk Altair Media Fomalhaut Zosma
Diadem Betelgeuse Jabbah Lesath Nembus Pherkad Procyon Alcyone Bellatrix
Castor Alcor Kochab Vega
*/
