/*
$ rustup run nightly cargo clippy
Compiling clippy v0.1.0 (file:///home/santi/Escritorio/ProyectoIDEA/clippy)
Finished dev [unoptimized + debuginfo] target(s) in 1.31 secs
$ rustc santiyepes.rs
$
*/

use std::io::{BufRead, BufReader};
use std::fs::File;

fn main() {
    let file = File::open("DATA.lst").expect("Error opening file");

    for buff in BufReader::new(file).lines() {
        let mut result_reading: &str = &buff.unwrap();
        let mut array: Vec<&str> = result_reading.split(' ').collect();
        for x in &array {
            let mut string_number: String = x.to_string();
            let mut number: i64 = string_number.parse::<i64>().unwrap();
            let mut multipli: f64 = number as f64 - f64::from(32);
            let mut result: f64 = multipli * f64::from(5) / f64::from(9);
            print!("{} ", (result).round());
        }
    }
}

/*
$ ./santiyepes
1 0 247 116 26 42 38 146 57 139 77 94 117 309
232 264 11 138 169 278 132 126 263 293 138
262 123 170 298 154 11 121 154 257 237
*/
