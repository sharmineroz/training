'''
mix credo --strict kergrau.exs
Checking 1 source file ...

Please report incorrect results: https://github.com/rrrene/credo/issues

Analysis took 0.3 seconds (0.08s to load, 0.3s running checks)
2 mods/funs, found no issues.

'''

{:ok, file} = File.read("DATA7.lst")
lines = String.split(file, " ", trim: true)
lines = List.delete_at(lines, 0)
defmodule Problem do
  def replacement(lines) do
    lines = String.replace(lines, "\n", "")
    lines = String.to_integer(lines)
    Kernel.round((lines - 32) * 5 / 9)
  end
end

Enum.each lines, fn(lines) -> IO.puts(Problem.replacement(lines)) end
'''
kergrau.exs
0 247 116 26 42 38 146 57 139 77 94 117 309 232 264 11 138 169 278 132 126 263
293 138 262 123 170 298 154 11 121 154 257 237
'''
