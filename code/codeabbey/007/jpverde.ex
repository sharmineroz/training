# mix credo
# Analysis took 0.04 seconds (0.01s to load, 0.03s running checks)
# 4 mods/funs, found no issues.
# $ iex
# iex(1)> c("jpverde.ex")

defmodule Temps do
  def data do
    {:ok, contents} = File.read("DATA.lst")
    nums = contents |> String.split([" ", "\r\n"], trim: true)
    dat = nums |> Enum.map(&String.to_integer/1)
    [_cant | vals] = dat

    for val <- vals do
        (val - 32) * 5 / 9
        |> Kernel.trunc
        |> to_string
    end
  end

  def celsius do
    temps = Sums.convert_temps
    IO.puts([Enum.join(temps, " ")])
  end
end

# iex(2)> Temps.celsius
# 0 246 116 26 42 38 146 57 139 77 93 116 308 231 264 11 137 169 278 132 126
# 262 292 138 262 122 170 297 154 10 121 154 257 237
# :ok
