
/*
tslint --init #linting config
tslint henrymejia2.ts #linting
*/
function getString(m) {
    let f = 479001600;
    let auxstr = "";
    let n = 12;
    const letters = ["A", "B", "C", "D", "E", "F", "G", "H",
        "I", "J", "K", "L"];
    while (n > 1) {
        f = f / n;
        const pos = Math.floor(m / f);
        m = m % f;
        auxstr = auxstr + letters[pos];
        letters.splice(pos, 1);
        n = n - 1;
    }
    auxstr = auxstr + letters[0];
    return auxstr;
}
const fs = require("fs");
fs.readFile(__dirname + "/DATA.lst", (err, data) => {
    if (err) {
        throw err;
    }
    const inputs = data.toString().split("\n");
    const T = inputs[0];
    let outputs = "";
    for (let index = 1; index <= T; index++) {
        outputs = outputs + " " + getString(inputs[index]);
    }
    outputs = outputs.substring(1, outputs.length);
    console.log(outputs);
});
/*
tsc henrymejia2.ts #get js
node henrymejia2.js #run js
KLFCEHBIJGAD LGDIFKEBJCHA JIFDHLCAKBGE DFABGHLJKIEC AFDBGJELKHCI
EHAKFBDCIJGL FALIEJDBCGHK KCEGJLHDAIBF FKIDCLGJAHEB EKBCLIJAHDFG
JGKHILCDABEF ICKJGBHDEALF GJCKABEHDFIL JAEKBDHICFGL FJCLGKIAEBDH
GFBDCAKLEIHJ GAECHDKBFLIJ CAHIKBJDGELF AEFIDJKBGCHL CDHEBLIKAJFG
LDJEKHAFGBIC DFJLEHICKBAG AJIFDHCBGELK
*/
