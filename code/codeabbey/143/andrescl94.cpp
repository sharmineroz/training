#include <iostream>

using namespace std;

void read_vals(int num, long *data){
    int offset=0;
    for(int i=0;i<num;i++){
        cin>>*(data+offset)>>*(data+offset+1);
        offset+=2;
    }
}

void extended_euclidean(int num, long *data){
    for(int i=0;i<num;i++){
        int offset=2*i,order;
        long int t=0,t1=1,r,r1,s=1,s1=0;
        if(*(data+offset)>*(data+offset+1)){
            r=*(data+offset); r1=*(data+offset+1);
            order=0;
        }
        else{
            r=*(data+offset+1); r1=*(data+offset);
            order=1;
        }
        while(r1!=0){
            long int q=r/r1, taux=t1, raux=r1, saux=s1;
            r1=r-q*r1; r=raux;
            if(r1==0){ break;
            }
            t1=t-q*t1; t=taux;
            s1=s-q*s1; s=saux;
        }
        if(order==0){
            cout<<r<<' '<<s1<<' '<<t1<<' ';    
        }
        else{ cout<<r<<' '<<t1<<' '<<s1<<' ';
        }
    }
}

int main(void){
    int num;
    cin>>num;
    long int data[num][2];
    read_vals(num,&data[0][0]);
    extended_euclidean(num,&data[0][0]);
    return 0;
}
