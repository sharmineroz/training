/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable no-console */

const screenWidth = 0.4;
const screenHeight = 0.3;
const screenDst = 0.5;
const eye = 1.0;
const screenw = 480.0;
const screenh = 360.0;

function computeCoordinate(objc, uscrinfo) {
  const ratio = uscrinfo[2] / objc[0];
  const scrx = Math.round((screenw / 2) -
    ((ratio * objc[1]) * (screenw / uscrinfo[0])));

  const scry = (screenh / 2) +
    ((ratio * uscrinfo[1 + 1 + 1]) * (screenh / uscrinfo[1]));

  const scrh = Math.round(scry -
    (ratio * objc[2] * (screenh / uscrinfo[1])));

  return [ scrx, Math.round(scry), scrh ];
}

function getScreenCoordinates(objcrd, uscrinfo) {
  const objscreen = objcrd.map((objc) => computeCoordinate(objc, uscrinfo));
  return objscreen;
}

function findScreenProjection(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const userscreen = [ screenWidth, screenHeight, screenDst, eye ];
  const dataLines = contents.split('\n');
  const objcrd = dataLines.slice(1).map((line) => line.split(' ').map(Number));
  const screencrd = getScreenCoordinates(objcrd, userscreen);
  screencrd.map((obju) => obju.map((crd) => console.log(crd)));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findScreenProjection(readerr, contents));
}

main();

/*
$ node badwolf10.js
240
330
30
69
266
9
*/
