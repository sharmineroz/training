%checkcode('vanemcb.m')

fileID = fopen('DATA.lst');
matCoef = fscanf(fileID,'%f %f %f',[3 inf]);
matCoef = matCoef';
s = size(matCoef,1);
cellSln = cell(s,2);
cellDisp = cell(1,s*2);

for i=1:s
  x1 = (-matCoef(i,2) + sqrt(matCoef(i,2)^2 - 4*matCoef(i,1)*matCoef(i,3)))...
  / (2*matCoef(i,1));
  x2 = (-matCoef(i,2) - sqrt(matCoef(i,2)^2 - 4*matCoef(i,1)*matCoef(i,3)))...
  / (2*matCoef(i,1));
  if matCoef(i,2)^2 - 4*matCoef(i,1)*matCoef(i,3) > 0 ||...
  matCoef(i,2)^2 - 4*matCoef(i,1)*matCoef(i,3) == 0
    x1 = num2str(real(x1));
    x2 = num2str(real(x2));
  else
    x1 = num2str(x1);
    x2 = num2str(x2);
  end
  cellSln{i,1} = x1;
  cellSln{i,2} = x2;
  cellSln{i,2}(1,end+1)=';';
end
cellSln = strjoin(cellSln');
disp(cellSln);

%vanemcb
%1 -10; 6 3; 6 -6; 3 -5; 10 7; 4+9i 4-9i; -3+8i -3-8i; -9+7i -9-7i;
%-7+1i -7-1i; 3 2; 10 -2; -5+7i -5-7i; -1+1i -1-1i;
