/*
$ java -jar scalastyle_2.12-1.0.0-batch.jar --config scalastyle_config.xml
neds.scala -q
Processed 1 file(s)
Found 0 errors
Found 0 warnings
Finished in 2583 ms
$ scalac neds.scala
*/

import scala.io.Source
import scala.collection.mutable.ListBuffer

object Neds extends App{

  def readData(): List[Int] = {
    val filename = "DATA.lst"
    var dat = new ListBuffer[Int]()
    for (line <- Source.fromFile(filename).getLines) {
      val aux = line.split(" ")
      for (e <- aux) {
        dat += e.toInt
      }
    }
    dat.toList
  }

  val data = readData()
  var out = ""
  var ones = 0
  var twos = 0
  var threes = 0
  var fours = 0
  var fives = 0
  var sixs = 0
  var pair = 0
  var three = 0
  var four = 0
  var yatch = 0
  var j = 0
  var straight = ""
  var flag_straight = true

  var i = 0
  for (x <- data) {
    if (i != 0) {
      x.toString match {
        case "1" => ones += 1
        case "2" => twos += 1
        case "3" => threes += 1
        case "4" => fours += 1
        case "5" => fives += 1
        case "6" => sixs += 1
      }
      if (i%5 == 0) {
        var dices: Vector[Int] = Vector(ones,twos,threes,fours,fives,sixs)
        pair = 0
        three = 0
        four = 0
        yatch = 0
        straight = ""
        flag_straight = true
        if (
        ones == 1 && twos == 1 && threes == 1 && fours == 1 && fives == 1) {
          straight = "small"
        } else if (
        sixs == 1 && twos == 1 && threes == 1 && fours == 1 && fives == 1) {
          straight = "big"
        } else {
          flag_straight = false
          for (el <- dices) {
            if (el.toFloat/2 == 1.0) {
              pair += 1
            } else if (el.toFloat/3 == 1.0) {
                three += 1
            } else if (el.toFloat/4 == 1.0) {
                four += 1
            } else if (el.toFloat/5 == 1.0) {
                yatch += 1
            }
          }
        }
        ones = 0
        twos = 0
        threes = 0
        fours = 0
        fives = 0
        sixs = 0
        if (flag_straight && straight == "small") {
          out += "small-straight "
        } else if (flag_straight && straight == "big") {
          out += "big-straight "
        } else if (pair == 1 && three == 1) {
          out += "full-house "
        } else if (pair == 2) {
          out += "two-pairs "
        } else if (yatch == 1) {
          out += "yacht "
        } else if (four == 1) {
          out += "four "
        } else if (three == 1 && pair != 1) {
          out += "three "
        } else if (pair == 1 && three != 1) {
          out += "pair "
        }else{
          out += "none "
        }
      }
    }
    i += 1
  }
print(out)
}

/*
$ scala Neds
four pair pair small-straight small-straight small-straight yacht
small-straight small-straight big-straight pair pair two-pairs
small-straight two-pairs pair pair big-straight small-straight
two-pairs pair two-pairs small-straight two-pairs two-pairs pair pair
*/
