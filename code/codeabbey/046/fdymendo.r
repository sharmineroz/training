winSelec <- matrix(c(1,2,3,4,5,6,7,8,9,1,4,7,2,5,8,3,6,9,1,5,9,3,5,7),ncol=8)
option <- function(pj, num){
  for(n in 1:ncol(winSelec)){
    pjWinner <- c(FALSE,FALSE,FALSE)
    r <- 1
    for(i in 1:num){
      if(pj[i] %in% winSelec[,n] ){
        pjWinner[r] <- TRUE
        r <- r + 1
      }
    }
    if(pjWinner[1] && pjWinner[2] && pjWinner[3]){
      return (TRUE)
    }
  }
  return (FALSE)
} 
  
winnerFunc <- function(a1,b1,a2,b2,a3,b3,a4,b4,a5){
  pj1 <- c(a1,a2,a3,a4,a5)
  pj2 <- c(b1,b2,b3,b4)
  turn <- 1
  for(i in 1:5){
    ######### PJ1 #####
    if(option(pj1,i)){
       return (cat(turn, "\n"))
    } else if(i<5){
      turn <- turn +1
      if(option(pj2,i)){
        return (cat(turn, "\n"))
      }
    }
    turn <- turn +1
  }
  return (car(0))
}

winnerFunc(3,2,9,6,4,8,5,1,7)
winnerFunc(5,7,1,9,4,3,8,6,2)
winnerFunc(9,7,6,5,3,2,4,8,1)
winnerFunc(6,2,7,1,5,4,9,8,3)
winnerFunc(1,9,7,8,3,6,5,4,2)
winnerFunc(1,3,9,6,8,7,2,4,5)
winnerFunc(8,7,6,9,4,5,1,2,3)
winnerFunc(2,3,9,7,8,4,5,1,6)
winnerFunc(7,9,3,5,4,1,2,8,6)
winnerFunc(7,9,4,8,6,2,3,1,5)
winnerFunc(9,7,4,8,5,6,3,1,2)
winnerFunc(2,4,6,7,8,9,1,5,3)
