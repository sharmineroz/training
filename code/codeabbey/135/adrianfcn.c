/*
$ splint adrianfcn.c
Splint 3.1.2 --- 01 Dec 2016

Finished checking --- no warnings
$ gcc adrianfcn.c -o output -lm
*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#define MAXBUFF 10000

static void searchprefix(char c, char s[]);
static void unionwords(char s[],char pharsebin[]);
static unsigned int bintodec(char s[]);
static void addzeros(char pharsebin[]);
static void encode();

struct prefix{
  char c;
  char bin[13];
};

int main(){
  encode();
  return 0;
}

void encode(){
  unsigned int i = 0,j = 0;
  char s[MAXBUFF] = "",pharsebin[MAXBUFF] = "",box[8];
  FILE *fp;
  fp = fopen("DATA.lst","r");
  /*@-nullpass@*/
  (void)fgets(s,MAXBUFF,fp);
  unionwords(s,pharsebin);
  addzeros(pharsebin);
  for(i = 0; i < (unsigned)strlen(pharsebin);i+=8){
    for(j=0; j < 8;j++){
      box[j] = pharsebin[i+j];
    }
    printf("%02X ", bintodec(box));
  }
  (void)fclose(fp);
}

void unionwords(char s1[],char phrasebin[]){
  unsigned int i;
  char s2[MAXBUFF];
  strcpy(s2,s1);
  for(i =0; i < (unsigned)strlen(s1)-1;i++){
    searchprefix(s1[i],s2);
    strcat(phrasebin,s2);
  }
}

void searchprefix(char c,char s[]){
  int i;

  struct prefix prefixs[] = {{' ',"11"},{'e',"101"},{'t',"1001"},{'o',"10001"},
  {'n', "10000"},{'a', "011"},{'s', "0101"},{'i', "01001"},{'r', "01000"},
  {'h', "0011"},{'d', "00101"},{'l', "001001"},{'!', "001000"},{'u',"00011"},
  {'c', "000101"},{'f', "000100"},{'m', "000011"},{'p', "0000101"},
  {'g', "0000100"},{'w', "0000011"},{'b', "0000010"},{'y', "0000001"},
  {'v', "00000001"},{'j', "000000001"},{'k', "0000000001"},{'x', "00000000001"}
  ,{'q', "000000000001"},{'z', "000000000000"}};
  for(i = 0; i < 28;i++){
    if(c == prefixs[i].c){
      strcpy(s,prefixs[i].bin);
    }
  }
}
void addzeros(char phrasebin[]){
  while(strlen(phrasebin)%8 != 0){
    strcat(phrasebin,"0");
  }
}
unsigned int bintodec(char binario[]){
  int n = 0,i,j;
  for(i = 7,j=0;i>-1;j++, i--){
    if(binario[i] !='0'){
      n+= pow(2,(double)j);
    }
  }
  return (unsigned)n;
}
/*
$ ./output
93 B8 51 82 25 22 E1 23 12 E4 1D CD AE 24 50 09 25 2D 86 69 5C 80 91 01 A8 84
50 BC C7 0A D5 72 09 60 D7 88 9A 43 0E 95 2E 6E 42 28 54 55 B0 97 29 8C 34 C3
93 B9 04 D0 89 52 5C 8D 17 7A 12 10 E7 4C 2A CA 43 60 89 BC C7 93 A1 EE 05 D3
0C 37 00 38 AD 6A F1 13 93 B8 2B 0A 22 67 88 99 12 84 B2 70 40 72 00 23 40 1C
81 22 8E 50 E0 A1 62 89 4C 02 63 5C 15 03 18 17 21 6B 50
*/
