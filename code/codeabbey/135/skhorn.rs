/*
$ rustup run nightly cargo clippy
Compiling backup v0.1.0 (file:///home/skhorn/Documents/programming/codeabbey/Rust/backup)
Finished dev [unoptimized + debuginfo] target(s) in 0.37 secs
$ rustc skhorn.rs
$
*/

//Read file
use std::collections::HashMap;
use std::io::{BufReader,BufRead};
use std::fs::File;

fn main() {
  let file = File::open("DATA.lst").unwrap();
  
  for line in BufReader::new(file).lines() {
    let cur_line: &str = &line.unwrap();
    
    let array: Vec<&str> = cur_line.split(' ').collect();
    let mut concat: String = Default::default();

    for i in 0 .. array.len() {
      if i > 0 &&  i < array.len() {
        concat.push_str("*");
        for item in array[i].chars() {
          concat.push_str(&item.to_string());
        }
      } else {
        for item in array[i].chars() {
          concat.push_str(&item.to_string());
        }
      }

    }
    
    let mut bin_set: String = binary_set(&concat.to_string());
 
    let mut chunk: String = Default::default();
    let mut chunk_arr: Vec<String> = Default::default(); 
    let mut chunk_count = 1;

    for char in bin_set.chars() {
      //println!("{}", char);
      if chunk_count < 9 {
        chunk += &char.to_string();
        //println!("{}", chunk_count);
      }
      if chunk_count == 8 { 
        //println!("{}", &chunk);
        chunk_arr.push(chunk);
        chunk_count = 0;
        chunk = Default::default();
      } 
      chunk_count += 1
    }  
    //println!("Ultimo chunk {}", chunk.len());
    if chunk.len() < 8  && chunk.len() != 1 {
      for _i in 0 .. 8-chunk.len() {
        chunk += "0";
      }
        chunk_arr.push(chunk);
    }
    //println!("{:?}", chunk_arr);
    
    let mut result: String = Default::default();
    for item in chunk_arr {
      let val: &str = &item;
      let n: u32 = u32::from_str_radix(val, 2).unwrap();
      let fmt = format!("{:01$X}", n, 2);
      //println!("{}", fmt);
      result.push(' ');
      result.push_str(&fmt.to_string());
    }  
    println!("{}", result);   
  }
}

// Converting the word to its binary representation
fn binary_set(word: &str) -> String {
  let _encoding_codes: HashMap<&str, &str> =
   [("*", "11"),
   ("a", "011"),
   ("b", "0000010"),
   ("c", "000101"),
   ("d", "00101"),
   ("e", "101"),
   ("f", "000100"),
   ("g", "0000100"),
   ("h", "0011"),
   ("i", "01001"),
   ("j", "000000001"),
   ("k", "0000000001"),
   ("l", "001001"),
   ("m", "000011"),
   ("n", "10000"),
   ("o", "10001"),
   ("p", "0000101"),
   ("q", "000000000001"),
   ("r", "01000"),
   ("s", "0101"),
   ("t", "1001"),
   ("u", "00011"),
   ("v", "00000001"),
   ("w", "0000011"),
   ("x", "00000000001"),
   ("y", "0000001"),
   ("z", "000000000000"),
   ("!", "001000")]
   .iter().cloned().collect();
  
  let mut result: String = Default::default();
  //println!("{}", word);
  for item in word.chars() {
    //println!("{}", item);
    for (key, value) in &_encoding_codes {
      //println!("{}: {}", key, value);
      if key == &item.to_string() {
        //println!("{} == {}", item, value);
        result += value;
      }
    }
  }
  //println!("{}", result);
  result
}

/*
$ ./skhorn.rs
93 B8 51 82 25 22 E1 23 12 E4 1D CD AE 24 50 09 25 2D 86 69 5C 80 91 01 A8 84 50 BC C7 0A D5 72 09 60 D7 88 9A 43 0E 95 2E 6E 42 28 54 55 B0 97 29 8C 34 C3 93 B9 04 D0 89 52 5C 8D 17 7A 12 10 E7 4C 2A CA 43 60 89 BC C7 93 A1 EE 05 D3 0C 37 00 38 AD 6A F1 13 93 B8 2B 0A 22 67 88 99 12 84 B2 70 40 72 00 23 40 1C 81 22 8E 50 E0 A1 62 89 4C 02 63 5C 15 03 18 17 21 6B 50
*/
