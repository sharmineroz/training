let t = Scanf.scanf "%d "(fun t ->  t);;
let x = ref (Scanf.scanf "%d\n"(fun x ->  x));;


let c = "bcdfghjklmnprstvwxz";;
let v = "aeiou";;
let pos = ref 0;;

let modulo x y =
  let result = x mod y in
  if result >= 0 then result
  else result + y
let cero = ref 0;;
let printArrayChar r = Array.iter (Printf.printf "%c ") r;;
let funny r c v = let word = String.make (Array.length r) 'a' in
                for i=0 to (Array.length r) - 1 do
                    if (i+1) mod 2 = 0 then
                    (
                        pos := r.(i) mod 5;
                        Bytes.set word i (Bytes.get v !pos)
                    )
                    else( pos := r.(i) mod 19;
                        Bytes.set word i (Bytes.get c !pos)
                        )
                done;
                word;;

let printArrayInt r = Array.iter (Printf.printf "%d ") r;;

let congruentialGenerator a c m n result = let j = ref 0 in
                                            let aux = ref 0 in
                                            while !n > 0 do
                                            aux := (a * !x + c) mod m;
                                            result.(!j) <- !aux;
                                            x := !aux ;
                                            n := !n - 1;
                                            j := !j + 1;
                                        done;
                                        result;;

let values = read_line () in
let intlist = List.map int_of_string(Str.split (Str.regexp " ") values) in

for i=0 to (List.length intlist) - 1 do
    let n =  List.nth intlist i in
    let result = Array.make n 0 in
    let r = congruentialGenerator 445 700001 2097152 (ref n) result in
    let z = funny r c v in
    Printf.printf "%s " z
done;;
