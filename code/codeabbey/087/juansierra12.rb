# frozen_string_literal: true

# $ rubocop juansierra12.rb

# Node class that holds every value and sons
class Node
  attr_reader :value, :right_son, :left_son
  attr_writer :right_son, :left_son

  def initialize(value)
    @value = value
    @right_son = nil
    @left_son = nil
  end

  def print
    '(' + (!@left_son ? '-' : @left_son.print) \
    + ',' + @value.to_s + ',' \
    + (!@right_son ? '-' : @right_son.print) + ')'
  end
end

# Class that creates the tree whit a root node
class Tree
  attr_reader :root
  attr_writer :root

  def initialize(root)
    @root = root
  end

  def assign_value(node, value)
    if node.value > value
      assign_left_node(node, value)
    else
      assign_right_node(node, value)
    end
  end

  def assign_left_node(node, value)
    if !node.left_son
      node.left_son = Node.new(value)
    else
      assign_value(node.left_son, value)
    end
  end

  def assign_right_node(node, value)
    if !node.right_son
      node.right_son = Node.new(value)
    else
      assign_value(node.right_son, value)
    end
  end
end

def read_file
  file = File.open('DATA.lst')
  _data_num = file.readline.to_i
  values = file.readline.split(' ')
  file.close
  values
end

def main
  values = read_file

  root = Node.new(values[0].to_i)
  tree = Tree.new(root)
  values.drop(1)
  values.each { |x| tree.assign_value(root, x.to_i) }

  puts tree.root.print
end

main if $PROGRAM_NAME == __FILE__

# $ ruby juansierra12.rb
# (((((((-,1,-),2,(-,3,-)),4,((-,5,-),6,(-,7,-))),8,-),9,(-,10,(-,11,-))),12,(
# (-,13,-),14,((-,15,(-,16,((-,17,-),18,(((-,19,((-,20,-),21,-)),22,-),23,-))))
# ,24,-))),25,(-,25,-))
