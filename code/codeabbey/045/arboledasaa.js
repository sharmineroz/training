/*
$ eslint jarboleda.js
$
*/

const suits = [ 'C', 'D', 'H', 'S' ];
const rankNumbers = [ 'A', '2', '3', '4', '5', '6', '7', '8', '9', 'T' ];
const rankNames = [ 'J', 'Q', 'K' ];
const ranks = rankNumbers.concat(rankNames);
const numberOfCards = 52;

function solveForNumber(cardValue) {
  const suitValue = Math.floor(cardValue / ranks.length);
  const rankValue = cardValue % ranks.length;
  const parsedNumber = `${ suits[suitValue] }${ ranks[rankValue] }`;
  return parsedNumber;
}

function generateList(currentIndex) {
  if (currentIndex >= numberOfCards) {
    return [];
  }
  return [ currentIndex ].concat(generateList(currentIndex + 1));
}

function swapPartTwo(currentArray, originalArray, from, tooooo, index) {
  if (index >= originalArray.length) {
    return [];
  } else if (index === from) {
    return currentArray.concat(originalArray[tooooo])
      .concat(swapPartTwo(currentArray, originalArray, from,
        tooooo, index + 1));
  } else if (index === tooooo) {
    return currentArray.concat(originalArray[from])
      .concat(swapPartTwo(currentArray, originalArray, from, tooooo,
        index + 1));
  }
  return currentArray.concat(originalArray[index])
    .concat(swapPartTwo(currentArray, originalArray, from, tooooo, index + 1));
}

function swap(originalArray, from, tooooo) {
  if (from > tooooo) {
    return swap(originalArray, tooooo, from);
  }
  return swapPartTwo([], originalArray, from, tooooo, 0);
}

function swapper(list, swaps, index) {
  if (index >= numberOfCards) {
    return list;
  }
  const swaped = swap(list, index, swaps[index] % numberOfCards);
  return swapper(swaped, swaps, index + 1);
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const list = generateList(0);
  const splited = contents.split(' ').slice(0, numberOfCards);
  const filtered = splited.filter((word) => (word.length > 0));
  const swappedList = swapper(list, filtered, 0);
  const answer = swappedList.map((theItem) => (solveForNumber(theItem)))
    .join(' ');
  const output = process.stdout.write(`${ answer }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
H7 HT D6 DT SA C4 C7 C2 CJ C3 DQ S9 D3 H5 CT H4 H9 D4 H6 S4 D2 DK H8 CK SQ S8
 H3 C5 C8 D8 HK C9 DJ DA S5 CA ST S6 D7 SK D5 HQ H2 D9 S2 C6 S7 CQ SJ HA HJ S3


*/
