#load "str.cma";;
let bin_of_int d =
    if d = 0 then "0" else
  let rec aux acc d =
    if d = 0 then acc else
    aux (string_of_int (d land 1) :: acc) (d lsr 1)
  in
  String.concat "" (aux [] d)
  let rec sublist b e l =
  match l with
    [] -> failwith "sublist"
  | h :: t ->
     let tail = if e=0 then [] else sublist (b-1) (e-1) t in
     if b>0 then tail else h :: tail;;
let () = Scanf.scanf "%d" (fun can -> for i = 0 to can - 1 do
  let d = read_int () in let binario = (bin_of_int d) in let tmo = (String.length binario) in
    if tmo > 32 then let insList = List.map int_of_string(Str.split (Str.regexp "") binario) in let dataArray = Array.make 32 0 in
let nval = (sublist 31 62 insList)in (for i=0 to ((Array.length dataArray) -1) do
   dataArray.(i) <- (List.nth nval i);
  done); Printf.printf " %d " (Array.fold_left (+) 0 dataArray); else let dataArray = Array.make 32 0 in
  let insList = List.map int_of_string(Str.split (Str.regexp "") binario) in
  Printf.printf " ";
  let tam = List.length insList in let dif = 32-tam  in
  (for i=0 to ((tam) -1) do
   dataArray.(i+dif) <- (List.nth insList i)
  done);
  Printf.printf " %d " (Array.fold_left (+) 0 dataArray)

done)
