program maracutamaxarray; {Nombre del programa}

uses
  SysUtils;

var
  Num2, x, y: longint; {Declaro variables como enteras}

  vector, vectorcomp: array [1..300] of longint;

begin

  {Inicializo variables para prevenir error de compilacion}
  Num2 := 0;


  {Solicito variable de control}


  writeLn('Escriba los numeros separados por espacio:');


  y := 300;
  repeat

    Read(Num2);
    vector[y] := Num2;
    y := y - 1;

  until y = 0;

  vectorcomp[1] := vector[1];
  vectorcomp[2] := vector[1];

  for x := 300 downto 1 do
  begin
    if (vectorcomp[1] > vector[x]) then
    begin
      vectorcomp[1] := vector[x];

    end;

    if (vectorcomp[2] < vector[x]) then
    begin
      vectorcomp[2] := vector[x];

    end;

  end;


  for x := 2 downto 1 do
  begin
    Write(vectorcomp[x]);
    Write(' ');
  end;

  readLn();
  readLn();

end.
