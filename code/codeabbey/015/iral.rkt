;; $ racket -I typed/racket
;; $ raco exe --gui iral.rkt

#lang typed/racket

( define data ( open-output-file "DATA.lst") )

(let ([nums (map string->number (string-split (read-line data)))])
  (printf "~A ~A~%" (apply max nums) (apply min nums)))

( close-output-port data )

;; ./iral
;; 79241 -78703
