/*
 * $ cargo fmt   --all     -- --check
 * $ cargo build --release
 *  Finished release [optimized] target(s) in 0.01s
 */

/*
 * This is pretty tricky, the fundamental hypothesis is
 * that you can identify the xor between letters and spaces:
 *
 * ASCII       HEX  BIN
 * space       0x20 00100000
 * letter from 0x40 01000000
 *        to   0x7f 01111111
 *
 * basically look at the seventh bit
 *      letter(1) ^ letter(1) = 0 = garbage
 *      space(0)  ^ letter(1) = 1 = space confirmed <- success!
 *      space(0)  ^ space(0)  = 0 = garbage
 * Given a stream produces 1 when xor-ed with respect to the other streams
 * Then those positions are spaces 100% confirmed
 * And the keys at the position of those chars are revealed
 *    char[i] ^ 0x20 = KEY[i]
 */

/*
 * Then you can define a key overrider
 * And with human interaction decipher the message
 */

#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

use std::io::BufRead;

fn main() {
  // problem data
  let mut streams: Vec<Vec<usize>> = Vec::new();

  // load problem data from "DATA.lst"
  let mut i: usize = 0;
  let mut file = std::fs::File::open("DATA.lst").unwrap();
  for it_line in std::io::BufReader::new(file).lines() {
    let line = &it_line.unwrap();
    let mut params: Vec<&str> = line.split('.').collect();

    if i > 0 {
      streams.push(Vec::new());
      for mut p in &mut params {
        streams[i - 1].push(usize::from_str_radix(p, 16).ok().unwrap());
      }
    }
    i += 1;
  }
  let n_streams: usize = i - 1;

  let mut keys: Vec<usize> = Vec::new();
  keys.resize(100, std::usize::MAX);

  // load overrides from "overrides.lst" file
  i = 0;
  file = std::fs::File::open("overrides.lst").unwrap();
  for it_line in std::io::BufReader::new(file).lines() {
    let line = &it_line.unwrap();
    let mut params: Vec<char> = line.chars().collect();
    let mut k: usize = 0;
    for c in &params {
      if *c != '.' {
        keys[k] = to_usize(*c) ^ streams[i][k];
      }
      k += 1;
    }
    i += 1;
  }

  // compare streams[i] with streams[j] and look if streams[i][k] is a space
  for i in 0..n_streams {
    for k in 0..streams[i].len() {
      let mut is_space: bool = true;
      for j in 0..n_streams {
        if i == j {
          continue;
        }
        if k < streams[j].len() {
          let seventh: usize = ((streams[i][k] ^ streams[j][k]) >> 0x6) & 0x1;
          if seventh == 0 {
            is_space = false;
            break;
          }
        }
      }
      if is_space {
        keys[k] = streams[i][k] ^ 0x20;
      }
    }
  }

  // decipher the message with the known key up to this moment
  for i in 0..n_streams {
    for k in 0..streams[i].len() {
      if keys[k] != std::usize::MAX {
        let c: usize = streams[i][k] ^ keys[k];
        print!("{}", to_ascii(c));
      } else {
        print!(".");
      }
    }
    println!("");
  }
}

// get ascii char from a code
fn to_ascii(i: usize) -> char {
  match i {
    x @ 32...126 => x as u8 as char,
    _ => '.',
  }
}

// get ascii code for a char
fn to_usize(c: char) -> usize {
  // just brute force it
  for i in 32..127 {
    if to_ascii(i) == c {
      return i;
    }
  }
  return 0;
}

/*
 * $ touch overrides.lst
 *
 * This is a cooperative process between human, math and code:
 *
 * $ cargo run -q --release
 * .......h .h.. ..e....u.to..d.....w.en.........r............s ..d..s.rp.ti...
 * .......an.y..l..a....e.un..i.....r.um.........f............ P..f..y.sc.rc...
 * .......op.e..f..h....C.lo..e.....e.nl......... ............Th.. ..e.e .ni...
 * .......he.s..o..u....t.us..o.....r.sd.........r............co..t..u.io. a..
 * .......nt.n..i..a....t.on..t..... .ri.........h............wa..e..t.em.fr..
 * .......pr.s..t..i....f.Gr..t.....a.n .........o............ed..n..r.es.an.
 * .......in. ..T..a....s.un..t.....e.th.........f............le..o..h.ve.We
 * .......re.p..d..c....e. t.. .....b.en.........t............ju..i.. .nd.of
 * .......ck.T..a..f....p.ni..m.....o. a.........s............sh..l..c.mm.t
 * .......ol.e..h..p....i.al..a.....h.ch.........n............it..a..t.er
 * .......ch.n..d..o....g.t ..d.....s.en.........a............ly..l.
 *
 * Now I can see that this line can be completed by logic, so I modify it
 * on overrides.lst file:
 * .......op.e..f..h....C.lo..e.....e.nl......... ........... Th.. the.e .ni...
 *                                                           ^     ^^
 *
 * Now perform another round
 * $ cargo run -q --release
 *   ...
 *   .......he.s..o..u....t.us..o.....r.sd.........r........... co..titu.io. a..
 *   .......nt.n..i..a....t.on..t..... .ri.........h........... wa..ed t.em.fr..
 *   .......pr.s..t..i....f.Gr..t.....a.n .........o...........ted..njur.es.an.
 *   .......in. ..T..a....s.un..t.....e.th.........f...........ple..or h.ve.We
 *   .......re.p..d..c....e. t.. .....b.en.........t........... ju..ice .nd.of
 *   .......ck.T..a..f....p.ni..m.....o. a.........s........... sh..ld c.mm.t
 *   .......ol.e..h..p....i.al..a.....h.ch.........n...........wit..anot.er
 *   .......ch.n..d..o....g.t ..d.....s.en.........a...........gly..ll
 *
 * Note how the text is getting completed by the software...
 *
 * Now I can see that this line can be completed by logic on overrides file:
 * .......ol.e..h..p....i.al..a.....h.ch.........n...........with another
 *                                                              ^^    ^
 * Repeat many cycles code - human, it took me like 25 iterations, and finally:
 * $ cargo run -q --release
 *   ...
 *   with others to subject us to a jurisdiction foreign to our constitution and
 *   been wanting in attentions to our British brethren We have warned them from
 *   of the present King of Great Britain is a history of repeated injuries and
 *   may define a Tyrant is unfit to be the ruler of a free people Nor have We
 *   and correspondence They too have been deaf to the voice of justice and of
 *   by a mock Trial from punishment for any Murders which they should commit
 *   to dissolve the political bands which have connected them with another
 *   not be changed for light and transient causes and accordingly all
 *
 * The solution is the last line:
 *  not be changed for light and transient causes and accordingly all
 */
