/*
$ javac arboledasaa.java
0
$
*/
import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class arboledasaa {

  public static void main(String[] args) throws IOException {
    /*
     * The maze is given as a rectangular matrix of 0 and 1 characters.
     */
    boolean maze[][] = readMaze();

    Point to = new Point(0, 0);
// top-right, bottom-left and bottom-right
    String topRightpath = getPath(maze, new Point(maze[0].length - 1, 0), to);
    String bottomLeftpath = getPath(maze, new Point(0, maze.length - 1), to);
    String bottomRightpath = getPath(maze,
        new Point(maze[0].length - 1, maze.length - 1), to);

    String encodedTopRightpath = encode(topRightpath);
    String encodedBottomLeftpath = encode(bottomLeftpath);
    String encodedBottomRightpath = encode(bottomRightpath);

    System.out.println(encodedTopRightpath + " " + encodedBottomLeftpath + " "
        + encodedBottomRightpath);
    System.exit(0);
  }

  /**
   * do Run-length encoding extracted from
   * https://stackoverflow.com/questions/11442162/java-run-length-encoding this
   * is public knowledge, so it isn't copyright infringement see
   * https://en.wikipedia.org/wiki/Run-length_encoding this is no Plagiarism as
   * it is public knowledge
   *
   * @param string
   * @return
   */
  public static String encode(String source) {
    StringBuffer dest = new StringBuffer();
    for (int i = 0; i < source.length(); i++) {
      int runLength = 1;
      while (i + 1 < source.length()
          && source.charAt(i) == source.charAt(i + 1)) {
        runLength++;
        i++;
      }
      dest.append(runLength);
      dest.append(source.charAt(i));
    }
    return dest.toString();
  }

  private static String getPath(boolean[][] maze, Point from, Point to) {
// pre
    Stack<Point> points = new Stack<>();
    Stack<Character> path = new Stack<>();
// input
    points.push(from);
// processing
    if (getPath(maze, points, path, to)) {
      return concat(path);
    } else {
// return something, may be replaced with an exception
      return "path not found";
    }
  }

  private static String concat(Stack<Character> path) {

    String answer = "";
    while (!path.isEmpty()) {
      answer = path.pop() + answer;
    }

    return answer;
  }

  /*
   * This is the base step check if finished it is finished if on the top of
   * stack is the objective point
   */
  private static boolean isFiniched(Point current, Point objective) {
    return current.equals(objective);
  }

  /**
   * This is the recursive method
   *
   * @param maze
   * @param points
   * @param path
   * @param to
   * @return
   */
  private static boolean getPath(boolean[][] maze, Stack<Point> points,
      Stack<Character> path, Point to) {
    Point current = points.peek();
    if (isFiniched(current, to)) {
      return true;
    } else {
// pre
// up
      Point up = (Point) current.clone();
      up.translate(0, -1);
// left
      Point left = (Point) current.clone();
      left.translate(-1, 0);
// right
      Point right = (Point) current.clone();
      right.translate(1, 0);
// down
      Point down = (Point) current.clone();
      down.translate(0, 1);

// process
      if (isValid(maze, up, to, points)) {
        points.push(up);
        path.push('U');
        if (getPath(maze, points, path, to)) {
          return true;
        }
      }
      if (isValid(maze, left, to, points)) {
        points.push(left);
        path.push('L');
        if (getPath(maze, points, path, to)) {
          return true;
        }
      }
      if (isValid(maze, right, to, points)) {
        points.push(right);
        path.push('R');
        if (getPath(maze, points, path, to)) {
          return true;
        }
      }
      if (isValid(maze, down, to, points)) {
        points.push(down);
        path.push('D');
        if (getPath(maze, points, path, to)) {
          return true;
        }
      }

// pop from stacks and return false
      points.pop();
      path.pop();
      return false;

    }
  }

  private static boolean isValid(boolean[][] maze, Point nextPoint, Point to,
      Stack<Point> points) {

// check if the point is in the maze
    if (nextPoint.getX() >= maze[0].length || nextPoint.getX() < 0) {
      return false;
    } else if (nextPoint.getY() >= maze.length || nextPoint.getY() < 0) {
      return false;
    }
// check if it is a wall
    else if (!maze[(int) nextPoint.getY()][(int) nextPoint.getX()]) {
      return false;
    }
// check if its already used
    else if (points.contains(nextPoint)) {
      return false;
    } else {
      return true;
    }

  }

  private static boolean[][] readMaze() throws IOException {
    /*
     * variables
     */
    /*
     * the input stream
     */
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    /*
     * the width and height of the maze
     */
    int width = 0;
    int height = 0;
    /*
     * a temporal variables
     */
    String line;
    String array[];
    /*
     * output variable
     */
    boolean[][] maze;
    /*
     * 1. Input: Maze is given as a rectangular matrix of 0 and 1 characters.
     */
    /*
     * 1.1 will contain width and height of the maze in first line
     */
    line = in.readLine();
    if (line != null) {
      array = line.split(" ");
      width = Integer.parseInt(array[0]);
      height = Integer.parseInt(array[1]);
      maze = new boolean[height][width];
      for (int i = 0; i < height; i++) {
        line = in.readLine();
        if (line != null) {
          if (line.length() >= width) {
            line = line.substring(0, width);
            for (int j = 0; j < width; j++) {
              char currentChar = line.charAt(j);
              maze[i][j] = (currentChar == '1');
            }
          } else {
            throw new NullPointerException(
                "There were no enough columns in a row");
          }
        } else {
          throw new NullPointerException("There were no enough rows");
        }
      }
      return maze;
    } else {
      throw new NullPointerException("There was no input");
    }

  }

}
/*
$ cat DATA.lst | java arboledasaa
4D4L2D4R4D14L8U2L2D10L2D2L2U2L4U4L
> 2R2U6R2U2R4U4R2D6R2U2L2U2L4U4R6U2L2D10L2D2L2U2L4U4L
> 2U4L6U12L2U2L4U4R6U2L2D10L2D2L2U2L4U4L
*/
