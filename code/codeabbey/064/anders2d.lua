--[[
$ ./luacheck.exe anders2d.lua
Checking anders2d.lua               OK
Total: 0 warnings / 0 errors in 1 file
]]

require 'io'

local data
local size = {}

data = io.lines("DATA.lst")

local function split(s, delimiter)
  local splitted = {};
  for match in (s..delimiter):gmatch("(.-)"..delimiter) do
    table.insert(splitted, tonumber( match));
  end
  return splitted;
end

local function isWay(position,testCase)
  if( position[1] == 0 or position[2] == 0 or
    position[1] == size[2]+1 or position[2] == size[1]+1) then
    return false
  end

  if (testCase[position[1]][position[2]] == 1) then

    return true
  else

    return false
  end
end
local function search(curPosition,lastPosition,step,letterWay,testCase)
  if(curPosition[1] == 1 and curPosition[2] ==1) then
    letterWay[#letterWay+1] = nil

    return true
  end
  local nextPosition
  step  = step +  1

  local x
  local y
  for i=-1, 1 ,2 do
    for _ = 1 , 2 do
      if(x==0) then
        x = i
        y = 0
      else
        x=0
        y=i
      end
      nextPosition = {curPosition[1]+x , curPosition[2] + y}
      --~ x-1 up x+1 down  y+1 right y-1 left

      if(nextPosition[1] == lastPosition[1] and
         nextPosition[2] == lastPosition[2]) then
         io.write('')

      elseif(isWay(nextPosition,testCase)) then
        local letter
        if x == -1 then
          letter = 'U'
          elseif x == 1 then
            letter = 'D'
          elseif y == -1 then
            letter = 'L'
          elseif y == 1 then
            letter = 'R'
        end
        letterWay[step] = letter
        if search(nextPosition,curPosition,step,letterWay,testCase)then
          return true
        end
      end
    end
  end
  return false
end

local function printResArray(way)
  local iterator = 1
  local lastLetter = way[1]
  local countLetter = 1
  repeat
    iterator = iterator + 1
    if lastLetter == way[iterator] then
      countLetter = countLetter + 1
    else


      io.write(countLetter..lastLetter)
      lastLetter=way[iterator]
      countLetter=1

    end
  until way[iterator]==nil
  io.write(" ")
end

local function initSearch(testCase)

  local way = {}
  local positionA,positionB,positionC
  positionA = { 1 , size[1]}
  positionB = {size[2] , 1 }
  positionC = {size[2],size[1]}

  search(positionA, {0,0} , 0 ,way,testCase)

  printResArray(way)
  way = {}
  search(positionB, {0,0} , 0 ,way,testCase)

  printResArray(way)
  way = {}
  search(positionC, {0,0} , 0 ,way,testCase)

  printResArray(way)

end

local function main()
  local testCase = {}
  for line in data do
    if (size[1] == nil) then
      size = split(line," ")
    else
      local arrayTmp= {}
      for i = 1, #line do
        arrayTmp[i] = tonumber(line:sub(i, i))
      end
      table.insert(testCase, arrayTmp)
    end
  end


  initSearch(testCase)

end


main()

--[[
$ lua -e "io.stdout:setvbuf 'no'" "anders2d.lua"
4D4L2D4R4D14L8U2L2D10L2D2L2U2L4U4L
2R2U6R2U2R4U4R2D6R2U2L2U2L4U4R6U2L2D10L2D2L2U2L4U4L
2U4L6U12L2U2L4U4R6U2L2D10L2D2L2U2L4U4L
]]
