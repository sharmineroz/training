#!/usr/bin/perl
# $perl dferrans.pl #perl has built-in linting
# use strict; use warnings;
use strict;
use warnings;

my $counter = 0;
my $finalresult = 0;
my $symbol = "";
my $brokerfee = 0;
foreach my $line ( <STDIN> ){
    my $answer = $line ;
    if ($counter > 0){

       my @words = split(/ /, $answer);
       my $sum = 0;
       for (@words[1..14]){
         $sum += $_;
       }
       my $average = $sum / 14;
       my $sum_dev = 0;

       for (@words[1..14]){
         $sum_dev =  $sum_dev + (($_ - $average)** 2);
       }

       my $stdev = sqrt( ($sum_dev /14) );
       my $trader_fee = $average * 0.01;
       my $ratio = $trader_fee / $stdev;
       if( $ratio > $finalresult){
         $finalresult = $ratio;
         $symbol = $words[0];
       }

       $average = 0;
       $sum_dev = 0;
    }

    $counter ++;
}

print "$symbol\n";
# $ perl dferrans.pl < DATA.lst
# GOLD
