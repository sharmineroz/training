/*
 * Problem #16 Average of an array
 */
var fs = require("fs");
var array = [];
fs.readFileSync(process.argv[2]).toString().split("\n").forEach(function (line){

    if(line.length > 3)
    {
        var data = [];
        data = line.split(" ");
        average(data);
    }

    function average(data) {

        var total_average = 0;
        data.forEach(function(item){
            var value = parseInt(item);
            if(value > 0)
            {
                total_average += value;
            }
        });
        total_average = total_average/(data.length-1);
        var rounded_value = rounding(total_average);
        array.push(rounded_value);
    }

    function rounding(value){
        
        var average_val = value;
        var mod = average_val%2;
        if( mod > 0.5)
        {
            return parseInt(average_val + 0.5);
        }
        else
        {
            return parseInt(average_val);
        }
    
    }
});
var str_out = "";
for (var item in array)
{
    str_out += array[item].toString()+" ";
}

console.log(str_out);
