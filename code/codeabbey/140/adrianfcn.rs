/*
$ rustfmt adrianfcn.rs
$ rustc adrianfcn.rs
*/

extern crate num_bigint;

use num_bigint::{BigUint,ToBigUint};
use std::fs::File;
use std::io::prelude::*;

fn factorial(x: u32) -> BigUint {
  let mut f: BigUint = 1_i32.to_biguint().unwrap();
  let limit = x + 1;
  for _i in 1..limit {
    f = &f * _i;
  }
  f
}

fn catalan_number(n: u32) -> BigUint {
  let num: BigUint = factorial(2 * n) / (factorial(n + 1) * factorial(n));
  num
}

fn main() -> std::io::Result<()> {
  let mut file = File::open("./DATA.lst")?;
  let mut contents = String::new();
  file.read_to_string(&mut contents)?;
  let lines = contents.lines();
  for line in lines {
    println!("{}", catalan_number(line.trim().parse::<u32>().unwrap()));
  }
  Ok(())
}
/*
$ ./adrianfcn
1834904322881454664759826702330511225520609976425374013948309426252005637
3623915511012465086486125017414100399828668403505507814261949803503626048
3183461448532980832227612734067434506825209299266076407430372419038260603
2680640
*/
