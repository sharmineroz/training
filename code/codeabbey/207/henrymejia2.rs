
/*
rustfmt henrymejia2.rs #format code instead of linting
rustc henrymejia2.rs #compilation
*/

use std::fs;

fn compare(a: usize, b: usize, arr: &[u8], sizes: usize) -> bool {
    if arr[a] > arr[b] {
        true
    }
    else if arr[a] < arr[b] {
        false
    }
    else if sizes > (a + 1) {
        if sizes > (b + 1) {
            compare(a + 1, b + 1, &arr, sizes)
        }
        else {
            true
        }
    }
    else {
        false
    }
}

fn get_position(n: usize, arr: &[u8], sizes: usize) -> u8 {
    let mut count = 0;
    for i in 0..sizes {
        if compare(sizes - n, i, &arr, sizes) {
            count += 1;
        }
    }
    count
}

fn get_index(vecs: &[u8], elem: usize) -> usize {
    for i in 0..1_000_000 {
        if vecs[i] == elem as u8 {
            return i;
        }
    }
    0
}

fn get_suffix_array(arr: &[u8], sizes: usize) -> String {
    let mut string = String::new();
    let mut result = String::new();
    let mut vecs: [u8; 1_000_000] = [0; 1_000_000];
    for i in 1..=sizes {
        if i > 1 {
            string.push_str(" ");
        }
        vecs[i - 1] = get_position(i, &arr, sizes);
        string.push_str(&get_position(i, &arr, sizes).to_string());
    }

    for i in 0..sizes {
        if i > 0 {
            result.push_str(" ");
        }
        result.push_str(&(sizes - get_index(&vecs, i) - 1).to_string());
    }
    result
}

fn main() {
    let filename = "DATA.lst";
    let string = fs::read_to_string(filename).expect(
        "Something went wrong reading the file");
    let inmbytes = string.as_bytes();
    let bytes: &mut [u8] = &mut [0; 10000];
    let sizes = inmbytes.len();
    for i in 0..sizes {
        bytes[i] = inmbytes[i];
    }
    let answer: String = get_suffix_array(&bytes, sizes);
    println!("{}", answer);
}
/*
./henrymejia2
222 174 115 97 149 22 5 64 143 164 83 47 200 45 188 105 221
114 4 113 3 112 92 155 184 102 38 93 29 196 156 168 159 107
73 76 33 52 36 214 141 1 21 82 183 195 58 138 67 151 14 192
99 95 181 26 60 161 109 71 125 87 175 116 111 51 98 59 207
185 120 208 166 24 103 7 139 16 39 163 199 187 75 66 150 94
70 86 23 6 186 65 30 31 43 68 90 56 197 78 144 127 152 205
11 32 213 0 13 191 15 55 130 157 202 121 209 218 131 193 169 147 100 179 96
63 44 167 158 20 182 137 160 108 50 165 74 69 89 77 204 217 19 203 122 27 34
84 9 48 123 53 61 91 28 35 57 25 119 162 198 85 42 10 201 146 216 134 210 219
177 135 132 79 46 104 37 81 194 110 129 49 18 8 118 41 145 215 128 170 153 211
189 171 173 148 142 220 2 154 101 106 72 140 180 124 206 126 212 12 190 54 178
62 136 88 133 176 80 17 117 40 172
*/
