--[[
$ luacheck anders2d.lua
Checking anders2d.lua                             OK
Total: 0 warnings / 0 errors in 1 file
]]

local function strToTable(str)
  local tmpTable={}
  str:gsub(".", function(c)
                  local tmpC  = {c, 1, 0}
                  table.insert(tmpTable,tmpC)
                end)
  return tmpTable
end

local function setCount(tmpTable)
  local acom = 0
  for i = 1, #tmpTable do
    acom = acom + tmpTable[i][2]
    tmpTable[i][3] = acom
  end
end

local function group(tmpTable)
  local iterator = 1
  while iterator <= #tmpTable do

    local iterator2 = iterator + 1
    while iterator2 <= #tmpTable do
      if (tmpTable[iterator][1] == tmpTable[iterator2][1]) then
        tmpTable[iterator][2] = tmpTable[iterator][2] + 1
        table.remove(tmpTable, iterator2)
      else
        iterator2 = iterator2 + 1
      end

    end

    iterator = iterator + 1

  end

  return tmpTable
end


local function reverse(tmpTable)
  local newTable = {}
  for i = #tmpTable ,1 ,-1 do
    table.insert(newTable,tmpTable[i])
  end
  return newTable
end

local function resetCountTable(tmpTable, countToRemove)
  for i = 1, #tmpTable do
    tmpTable[i][3] = tmpTable[i][3] - countToRemove
  end
end

local function getSplitInTheMiddle(tmpTable)
  local sizeTable = #tmpTable

  local sumRightToLeft = 0
  local leftTable = {}
  local rightTable = {}
  local removeNumber = 0

  if (#tmpTable > 1 ) then
    for i = sizeTable, 1, -1 do
      sumRightToLeft = sumRightToLeft + tmpTable[i][2]
      if sumRightToLeft > tmpTable[i][3] then
        table.insert(leftTable,tmpTable[i])
      else

        table.insert(rightTable,tmpTable[i])
      end

    end
  end

  leftTable = reverse(leftTable)
  rightTable = reverse(rightTable)

  if ( leftTable[#leftTable] ~= nil ) then

    removeNumber = leftTable[#leftTable][3]
  end
  resetCountTable(rightTable,removeNumber)
  return {leftTable, rightTable}
end

local function orderLogic(a,b)
  if a[2] ~= b[2] then
    return a[2] > b[2]
  end

  return a[1] < b[1]
end

local function printTableInLine(tmpTable)
  for i = 1, #tmpTable do
    if(tmpTable[i] ~= nil) then
      io.write(tmpTable[i])
     end
  end
end

local function recursiveShannonFano(tmpRightLeftTable, level, way)

  level = level + 1

  local leftTable = tmpRightLeftTable[1]
  local rightTable = tmpRightLeftTable[2]

  if #leftTable > 0 then
      way[level] = 'O'
      way[level+1] = nil
      if(#leftTable == 1) then
        io.write(string.byte(leftTable[1][1]))
        io.write(" ")
        printTableInLine(way)
        io.write(" ")
      else
        local leftTableSplited = getSplitInTheMiddle(leftTable)
        recursiveShannonFano(leftTableSplited,level,way)
      end

  end

  if rightTable[1] ~= nil then

      way[level] = 'I'
      way[level+1] = nil

      if(#rightTable == 1) then
         io.write(string.byte(rightTable[1][1]))
         io.write(" ")
         printTableInLine(way)
         io.write(" ")
      else

     local  rightTableSplited = getSplitInTheMiddle(rightTable)
      recursiveShannonFano(rightTableSplited, level, way)

    end
  end
  return true
end

local function main()
  local data = io.lines("DATA.lst")
  local text
  for line in data do
    text = line
    break
  end
  local shannonTable = strToTable(text)
  local way = {}
  group(shannonTable)
  table.sort(shannonTable,orderLogic)
  setCount(shannonTable)

  local RightLeftTable = getSplitInTheMiddle(shannonTable)

  recursiveShannonFano(RightLeftTable, 1, way)

end

main()
--[[
$ lua anders2d.lua
32 OOO 101 OOI 116 OIOO 97 OIOI 115 OIIO 105 OIII 104 IOOO 114 IOOI 111 IOIO
100 IOIIO 110 IOIII 99 IIOOO 117 IIOOI 102 IIOIOO 119 IIOIOI 121 IIOIIO 34
IIOIII 45 IIIOOO 98 IIIOOIO 103 IIIOOII 108 IIIOIO 112 IIIOIIO 44 IIIOIII
46 IIIIOOO 109 IIIIOOI 118 IIIIOIO 33 IIIIOII 59 IIIIIOOO 63 IIIIIOOI 73
IIIIIOIO 76 IIIIIOIIO 82 IIIIIOIII 84 IIIIIIOO 87 IIIIIIOIO 106 IIIIIIOII
107 IIIIIIIO 113 IIIIIIIIO 120 IIIIIIIII
]]
