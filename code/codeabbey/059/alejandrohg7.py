#!/usr/bin/env python3
'''
$pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''


def open_file():
    '''This script open and reads a file '''
    with open('DATA.lst', 'r') as opened_file:
        code_guesses = []
        for line in opened_file:
            code_guesses.append(line.split())
    return int(code_guesses[0][0]), code_guesses[1]


def hints(alice_code, barbara_guesses):
    """This script gives hints for guess the code"""
    code = str(alice_code)
    guesses = barbara_guesses
    pre_hint = []
    for guess in guesses:
        number_lst = [number for number in guess]
        same_index = []
        other_index = []
        for single_number in number_lst:
            guess_finder = guess.find(single_number)
            code_finder = code.find(single_number)
            if guess_finder == code_finder:
                same_index.append(single_number)
            elif single_number in code:
                other_index.append(single_number)
            else:
                pass
        len_same = len(same_index)
        len_other = len(other_index)
        pre_hint.append((len_same, len_other))
    pre_hint = str(" ".join(map(str, pre_hint))).replace("(", "")
    pre_hint = pre_hint.replace(")", "").replace(", ", "-")
    return pre_hint


hints(open_file()[0], open_file()[1])
# $ python3 alejandrohg7.py
# 0-2 0-2 0-0 1-1 1-1 0-1 0-0 0-1 0-0 0-0
