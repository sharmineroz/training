
print_endline "Ingrese la Cantidad de casos a verificar";;


let rec sumadig digito = function
  | 0 -> digito
  | cantidad -> sumadig (digito + (cantidad mod 10)) (cantidad / 10)

let oper cant = Scanf.scanf "%d %d %d " (fun a b c -> a * b + c)

let () =
  Scanf.scanf "%d " (fun cantidad ->
                      for i = 0 to cantidad - 1 do oper i
                        |> sumadig 0
                        |> Printf.printf "%d "
                      done)
