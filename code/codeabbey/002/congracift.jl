#=
 $julia
 julia> using Lint
 julia> lintfile("congracift.jl")
=#

function sum_loop(numbers)
  solved = 0
  array_numbers = split(numbers)
  for i in array_numbers
    solved = parse(Int64, i) + solved
  end
  println(solved)
end

open("DATA.lst") do file
  flag = false
  for ln in eachline(file)
    if flag == false
      flag = true
        continue
    end
    sum_loop(ln)
  end
end

# $julia congracift.jl
# 31752
