#=
 $julia
 julia> using Lint
 julia> lintfile("jdiego10.jl")
=#

open("DATA.lst") do file
  flag = false
  for ln in eachline(file)
     function hero(array)
     x1=array[:,1]
     y1=array[:,2]
     x2=array[:,3]
     y2=array[:,4]
     x3=array[:,5]
     y3=array[:,6]
     if flag == false
         flag = true
         continue
      end
     for i=1:length(x1)
         AB=sqrt((x1[i]-x2[i])^2+(y1[i]-y2[i])^2)
         AC=sqrt((x1[i]-x3[i])^2+(y1[i]-y3[i])^2)
         BC=sqrt((x2[i]-x3[i])^2+(y2[i]-y3[i])^2)
         Area=1/4*sqrt(4*(BC^2*AC^2)-(BC^2+AC^2-AB^2)^2)
        @printf("%f ",Area)
    end
end

#=
 $julia jdiego10.jl
 8008545.500000013 9597811.000000004 11915667.000000007 11450170.999999996
 15066083.999999994 2427172.5000000005 3265796.999999956 31022920
 492795.0000000145 5095621.000000001 4526801.999999999
 8112862.499999996 1371713.0000000023 1036640.9999999971
 7015461.499999996 20091288.499999996
=#
