/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable no-console */

const screenw = 0.4;
const usertoscreend = 0.5;
const maxdtu = 60;
const tlr = 9;

function removePlayerOffset(objxy, playerinit) {
  const objshift = objxy.map((objc) => objc.map((crd, indx) =>
    crd - playerinit[indx]));
  return objshift;
}

function rotateObjectsAroundZero(objxy, playerinit) {
  const objshifted = removePlayerOffset(objxy, playerinit);
  const angle = playerinit[1 + 1];
  const rotobjxy = objshifted.map((objc) => {
    const xrot = (objc[0] * Math.cos(angle)) + (objc[1] * Math.sin(angle));
    const yrot = (-objc[0] * Math.sin(angle)) + (objc[1] * Math.cos(angle));
    return [ xrot.toFixed(tlr), yrot.toFixed(tlr) ];
  });
  return rotobjxy;
}

function getUserSeenObjects(objxy, playerinit, userscreen) {
  const objrotated = rotateObjectsAroundZero(objxy, playerinit);
  const viewspanhalf = Math.atan(userscreen[0] / (2 * userscreen[1]));
  const objseen = objrotated.filter((objr) => {
    const objang = Math.atan2(objr[1], objr[0]);
    if (objang > (-viewspanhalf) && objang < viewspanhalf) {
      const dtouser = Math.sqrt(Math.pow(objr[0], 2) + Math.pow(objr[1], 2));
      if (dtouser > userscreen[1] && dtouser < userscreen[2]) {
        return true;
      }
    }
    return false;
  });
  return objseen;
}

function getUserScene(objxy, playerinit, userscreen) {
  const objuser = getUserSeenObjects(objxy, playerinit, userscreen);
  return objuser;
}

function findSceneObjects(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const userscreen = [ screenw, usertoscreend, maxdtu ];
  const dataLines = contents.split('\n');
  const playerinit = dataLines.slice(1).slice(0, 1)[0].split(' ').map(Number);
  const objxy = dataLines.slice(2).map((objc) => objc.split(' ').map(Number));
  const objuser = getUserScene(objxy, playerinit, userscreen);
  objuser.map((obju) => obju.map((crd) => console.log(crd)));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findSceneObjects(readerr, contents));
}

main();

/*
$ node badwolf10.js
14.142135624
-0.000025973
*/
