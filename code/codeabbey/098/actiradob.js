/*
$ eslint actiradob.js
$
*/

function toRad(azimuth) {
  const degA = 360;
  const degB = 270;
  const degC = 180;
  const degrees = (degA - azimuth) - degB;
  const rad = degrees * (Math.PI / degC);
  return rad;
}

function moveP(pos, rad, feet) {
  pos[0] += feet * Math.cos(rad);
  pos[1] += feet * Math.sin(rad);
  return pos;
}

function pirat(miss, file) {
  const lines = file.split(/\r\n|\r|\n/g);
  let pos = [ 0, 0 ];
  for (let i = 1; i < lines.length - 1; i += 1) {
    const order = lines[i].split(' ');
    const posAzimuth = 5;
    const numAzimuth = parseInt(order[posAzimuth], 10);
    const numFeet = parseInt(order[1], 10);
    pos = moveP(pos, toRad(numAzimuth), numFeet);
  }
  process.stdout.write(`${ Math.round(pos[0]) } ${ Math.round(pos[1]) }`);
}

const fileS = require('fs');

function fileLoad() {
  return fileS.readFile('DATA.lst', 'utf8', (miss, file) => pirat(miss, file));
}

fileLoad();

/*
$ node actiradob.js
-190 2218
*/
