/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

// Unnecesary or conflicting rules
/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable no-console */
/* eslint-disable array-bracket-newline */
/* eslint-disable array-element-newline */

const deltat = 1e-9;
const pideg = 180;
const twopideg = 180;

function computeFunction(xydata, fparam) {
  const [ xval, yval ] = xydata;
  const [ para, parb, parc ] = fparam;
  const termab = Math.pow(xval - para, 2) + Math.pow(yval - parb, 2);
  const termc = parc * Math.exp(-(Math.pow(xval + para, 2) +
    Math.pow(yval + parb, 2)));
  return termab + termc;
}

function computeGradientAngle(xydata, fparam, delta) {
  const diffx = (computeFunction([ xydata[0] + delta, xydata[1] ], fparam) -
    computeFunction(xydata, fparam)) / delta;
  const diffy = (computeFunction([ xydata[0], xydata[1] + delta ], fparam) -
    computeFunction(xydata, fparam)) / delta;
  const angle = Math.round(Math.atan2(diffy, diffx) * (pideg / Math.PI)) +
    pideg;
  const nangle = angle < 0 ? twopideg + angle : angle;
  return nangle;
}

function findGradient(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const fparam = dataLines.slice(0, 1)[0].split(' ').slice(1).map(Number);
  const grdata = dataLines.slice(1).map((line) =>
    line.split(' ').map(Number));
  const gangles = grdata.map((xypair) =>
    computeGradientAngle(xypair, fparam, deltat));
  gangles.map((val) => console.log(val));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findGradient(readerr, contents));
}

main();

/*
$ node badwolf10.js
222 246 211 212 234 254 19 214 172 234 244 254 175 213
*/
