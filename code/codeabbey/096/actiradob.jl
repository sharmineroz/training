# $ julia
#
#   _       _ _(_)_     |  A fresh approach to technical computing
#  (_)     | (_) (_)    |  Documentation: https://docs.julialang.org
#   _ _   _| |_  __ _   |  Type "?help" for help.
#  | | | | | | |/ _` |  |
#  | | |_| | | | (_| |  |  Version 0.6.4 (2018-07-09 19:09 UTC)
# _/ |\__'_|_|_|\__'_|  |  Official http://julialang.org/ release
#|__/                   |  x86_64-w64-mingw32
# $ using Lint
# $ length(lintfile("actiradob.jl"))
# 0

function move(snakeA, foodA, action, pos, total, crash)
  if action == "U"
    pos = "U"
  elseif action == "D"
    pos = "D"
  elseif action == "L"
    pos = "L"
  elseif action == "R"
    pos = "R"
  else
    steps = parse(Int64,action)
    if pos == "U"
      while !crash && steps > 0
        if in(snakeA[1]+[-1,0],snakeA)
          unshift!(snakeA,snakeA[1]+[-1,0])
          crash = true
        elseif in(snakeA[1]+[-1,0],foodA)
          unshift!(snakeA,snakeA[1]+[-1,0])
          setdiff(foodA,[snakeA[1]])
          total = total+1
          steps = steps-1
        else
          unshift!(snakeA,snakeA[1]+[-1,0])
          pop!(snakeA)
          total = total+1
          steps = steps-1
        end
      end
    elseif pos == "D"
      while !crash && steps > 0
        if in(snakeA[1]+[1,0],snakeA)
          crash = true
          unshift!(snakeA,snakeA[1]+[1,0])
        elseif in(snakeA[1]+[1,0],foodA)
          unshift!(snakeA,snakeA[1]+[1,0])
          setdiff(foodA,[snakeA[1]])
          total = total+1
          steps = steps-1
        else
          unshift!(snakeA,snakeA[1]+[1,0])
          pop!(snakeA)
          total = total+1
          steps = steps-1
        end
      end
    elseif pos == "L"
      while !crash && steps > 0
        if in(snakeA[1]+[0,-1],snakeA)
          crash = true
          unshift!(snakeA,snakeA[1]+[0,-1])
        elseif in(snakeA[1]+[0,-1],foodA)
          unshift!(snakeA,snakeA[1]+[0,-1])
          setdiff(foodA,[snakeA[1]])
          total = total+1
          steps = steps-1
        else
          unshift!(snakeA,snakeA[1]+[0,-1])
          pop!(snakeA)
          total = total+1
          steps = steps-1
        end
      end
    else
      while !crash && steps > 0
        if in(snakeA[1]+[0,1],snakeA)
          crash = true
          unshift!(snakeA,snakeA[1]+[0,1])
        elseif in(snakeA[1]+[0,1],foodA)
          unshift!(snakeA,snakeA[1]+[0,1])
          setdiff(foodA,[snakeA[1]])
          total = total+1
          steps = steps-1
        else
          unshift!(snakeA,snakeA[1]+[0,1])
          pop!(snakeA)
          total = total+1
          steps = steps-1
        end
      end
    end
  end
  return snakeA, foodA, pos, total, crash
end

function snake()
  b = Array{Array{String,1},1}(13)
  actions = Array{SubString{String},1}
  open("DATA.lst") do f
    for i = 1:13
      b[i] = split(readline(f))
    end
    actions = split(readline(f))
  end
  snakeA = [[1,3],[1,2],[1,1]]
  foodA = []
  for i = 1:13
    for j = 1:21
      if b[i][j] == "\$"
        push!(foodA,[i,j])
      end
    end
  end
  pos = "R"
  p = 1
  crash = false
  total = 0
  while !crash
    snakeA,foodA,pos,total,crash = move(snakeA,foodA,actions[p],pos,total,crash)
    p = p + 1
  end
  return string(snakeA[1][2]-1," ", snakeA[1][1]-1," ", total+1)
end

snake()

# $ include("actiradob.jl")
# "6 9 45"
