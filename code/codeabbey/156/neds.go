/*
$ gofmt -w neds.go
$ go vet neds.go
vet.exe: errors: 0; warnings: 0
$ go build -work neds.go
WORK=C:\Users\NEDS\AppData\Local\Temp\go-build005082498
*/

package main

import (
  "bufio"
  "fmt"
  "log"
  "os"
  "strconv"
  "strings"
)

func convString(el string) int {
  number, e := strconv.Atoi(el)
  if e != nil {
    fmt.Println(e)
  }
  return number
}

func readData() (int, []string) {
  var data []string
  file, err := os.Open("DATA.lst")
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()

  lines := bufio.NewScanner(file)
  for lines.Scan() {
    data = append(data, lines.Text())
  }

  if err := lines.Err(); err != nil {
    log.Fatal(err)
  }
  n := len(data)
  return n, data
}

func algorithm(number string) bool {
  nDig := len(number)
  sum := convString(string(number[nDig-1]))
  parity := nDig % 2
  for i := 0; i < nDig-1; i++ {
    digit := convString(string(number[i]))
    if i%2 == parity {
      digit = digit * 2
    }
    if digit > 9 {
      digit = digit - 9
    }
    sum = sum + digit
  }
  return sum%10 == 0
}

func restoreDigit(number string, n int) string {
  return strings.Replace(number, "?", strconv.Itoa(n), 1)
}

func swapDigit(number string, pair int) string {
  num := []byte(number)
  aux := num[pair]
  num[pair] = num[pair+1]
  num[pair+1] = aux
  return string(num)
}

func main() {
  var out string
  var newNumber string
  var complete bool
  n, data := readData()
  for i := 0; i < n; i++ {
    newNumber = data[i]
    complete = false
    if strings.Index(newNumber, "?") != -1 {
      newDigit := 0
      for !complete {
        newNumber = restoreDigit(data[i], newDigit)
        complete = algorithm(newNumber)
        if !complete && newDigit == 9 {
          complete = true
        }
        newDigit++
      }
    }else {
      pair := 0
      complete = algorithm(newNumber)
      if complete && string(newNumber[0]) == "0" {
        complete = false
      }
      for !complete {
        newNumber = swapDigit(data[i], pair)
        complete = algorithm(newNumber)
        if !complete && pair >= len(newNumber)-1 {
          newNumber = data[i]
          complete = algorithm(newNumber)
          if !complete {
            complete = true
          }
        }
        pair++
      }
    }
    out += newNumber + " "
  }
  fmt.Println(out)
}

/*
$ neds.exe
5971406908116158 7399233746090936 8891733674480349 1493692969784665
8952895656212556 1039487771668821 9800971134974224 6320831181825561
3755352032706455 4681051690023305 9782539125121550 6042024824938036
1707272994899069 8183190141506304 1515182024139224 2293684606322561
5323786318420687 3677635982563016 5113852867840743 8724891260239025
4870574786691546 6442934446426085 6566941041650605 3831032700441662
4904706462087967 3358385699350507 3319758665633866 2816202991376189
6826872662438481 5093888508349383 8973264034398451 9715121934005225
7533864765651880 2308899351638374 1011093846615334 8536299733311629
7806698656647247 2557443471641394 4780563168993124 1414926068258502
7505966416613898
*/
