-- luacheck oscardjuribe.lua
-- Checking oscardjuribe.lua                         OK
-- Total: 0 warnings / 0 errors in 1 file

--[[
Code to solve the problem Information Entropy
from codeabbey in lua
]]

local function contains(arr, value)
  -- method to check if array contains a value

  -- iterate over key array
  for k, _ in pairs(arr) do
    -- check if contain the value
    if k == value
    then
      return true
    end
  end
  return false
end

local function log2(x)
  -- method to calculate the log2 of a given number

  -- use change of base rule
  return math.log(x)/ math.log(2)
end

local function get_entropy(line)
  -- method to calculate the entropy for a given line

  local chars_frecuency = {}
  -- iterate over chars
  for c in line:gmatch('.') do
    -- check if current char is on the array
    if contains(chars_frecuency, c)
    then
      -- add one if it was
      chars_frecuency[c] = chars_frecuency[c] + 1
    else
      -- start frecuency at 1
      chars_frecuency[c] = 1
    end
  end

  -- store current entropy
  local entropy = 0
  -- var to store frecuency inside for loop
  local current_frecuency
  -- length of string
  local str_len = #line

  -- iterate over frecuency array
  for _, v in pairs(chars_frecuency) do
    -- calculate frecuency
    current_frecuency = v / str_len
    -- use entropy formula
    entropy = entropy + (current_frecuency * -log2(current_frecuency))
  end

  return entropy
end

-- read data from file
local file = io.open("DATA.lst", "r")
-- file as input
io.input(file)

-- read test cases
local test_cases = io.read('*n')

-- var to store current solution
local solution = ''
-- empty line
io.read()

-- store current line
local line

-- iterate over testcases
for _ = 1, test_cases do
  -- read the line
  line = io.read('*l')
  -- calculate the entropy
  solution = solution .. get_entropy(line) .. " "
end

print (solution)
-- $ lua oscardjuribe.lua
-- 3.8731406795131 3.0053148568943 3.3685225277282
-- 3.3921472236645 3.3423709931771 3.6732696895151
-- 3.1158340921632 3.2841837197792 3.192417846777
-- 3.461320140211 3.5511911744657 3.046439344671
-- 3.636842188131 3.0105709342685 3.0306390622296
-- 3.3502090290999 3.0105709342685 3.5160276412662
