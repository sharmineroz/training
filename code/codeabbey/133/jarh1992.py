"""
$ pylint jarh1992.py

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""
import math

LST = list(map(float, input().split(' ')))

W, H, X, Y, R, VX, VY, A = LST
V = math.hypot(VX, VY)
DT = 0.001
ANG_A = math.acos(VX/V)
ANG_B = math.asin(VY/V)
while V > 0.0:
    if X < R:
        VX = V * math.cos(ANG_A) * -1
        X += 2 * (R - X)
        ANG_A = math.acos(VX/V)
    elif X > W-R:
        VX = V * math.cos(ANG_A) * -1
        X -= 2 * (X - W + R)
        ANG_A = math.acos(VX/V)
    else:
        X += VX * DT
        VX = V * math.cos(ANG_A)

    if Y < R:
        VY = V * math.sin(ANG_B) * -1
        Y += 2 * (R - Y)
        ANG_B = math.asin(VY/V)
    elif Y > H-R:
        VY = V * math.sin(ANG_B) * -1
        Y -= 2 * (Y - H + R)
        ANG_B = math.asin(VY/V)
    else:
        Y += VY * DT
        VY = V * math.sin(ANG_B)

    V -= A * DT

print(round(X), round(Y))

# $ python jarh1992.py
# 500 250 50 50 10 86 86 5
# 136 176
