--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local case
local result = {}
local d = -1
for line in data do
  if d == -1 then
    case = tonumber(line)
    d = 1
  else
    local a = 0
    for token in string.gmatch(line, "[^%s]+") do
      result[a] = tonumber(token)
      a = a +1
    end
  end
end
local result2 = {}
for i = 1, case-2 do
  result2[i] = (result[i-1]+result[i]+result[i+1])/3
end
result2[0] = result[0]
result2[case-1] = result[case-1]
for i = 0, case-1 do
  print(result2[i])
end

--[[
$ lua mrsossa.lua
32.6 33.0 34.6 39.166666666667 41.466666666667 43.7 44.1
]]
