/**
 * $ dartanalyzer sebasbeleno.dart
 * Analyzing  sebasleno.dart
 * No issues found!
 */
import 'dart:io';
import 'package:path/path.dart' show dirname, join;

void main() {
  var file = File(join(dirname(Platform.script.path), 'DATA.lst'));

  file.readAsLines().then((List data) {
    var line = data[0];
    var testCases = int.parse(line);
    var strData = data[1];
    var strArrayData = strData.split(' ');
    var arrayData = strArrayData.map(double.parse).toList();
    var answer = [1.0];
    answer[0] = arrayData[0];

    for (var i = 1; i < testCases - 1; i++) {
      var a = arrayData[i - 1];
      var c = arrayData[i + 1];
      var b = arrayData[i];
      answer.add((a + b + c) / 3);
    }

    answer.add(arrayData[arrayData.length - 1]);

    print(answer);
  });
}

/**
 * dart sebasbeleno.dart
 * 32.6, 33.0, 34.6, 39.166666666666664, 41.46666666666667,
 * 43.699999999999996, 44.1
 */
