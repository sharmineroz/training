"""
    Lucky Tickets - Codeabbey 109
    >pylint data_reader.py
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""


class Ticket(object):
    """
        This class does represent both the ways of obtaining
        n tickets, and the total number of ways
    """
    def __init__(self, n_1, b_1):
        self.n_1 = int(n_1)
        self.b_1 = int(b_1)
        self.half = int(self.n_1/2)
        self.dp_matrix = []
        for i in range((self.b_1-1)*self.half+1):
            l_1 = []
            for j in range(self.half + 1):
                j = j  # Linters make you write dumb things
                l_1.append(-1)
            self.dp_matrix.append(l_1)
        for i in range(self.half + 1):
            self.dp_matrix[0][i] = 1
            if self.half > 0:
                self.dp_matrix[1][i] = i
        for i in range((self.b_1-1)*self.half+1):
            if i < self.b_1 and self.half > 0:
                self.dp_matrix[i][1] = 1
            if i != 0:
                self.dp_matrix[i][0] = 0

        # Let dp be the Dynamic Programming matrix where
        # Mij represents the number of ways of getting value i
        # using j digits.

    def lucky_tickets(self):
        """
            Get amount of lucky tickets given N and B
        """
        w_1 = 0
        for i in range((self.b_1-1)*self.half+1):
            n_1 = self.num_ways(self.half, i, self.b_1)
            w_1 += n_1*n_1
        return w_1*self.b_1 if self.n_1 % 2 else w_1

    def num_ways(self, n_1, v_1, b_1):
        """
            Get the numer of ways of representing
            a given value v, with n base b digits
        """
        if self.dp_matrix[v_1][n_1] != -1:
            return self.dp_matrix[v_1][n_1]

        w_1 = 0
        for i in range(v_1 + 1 if v_1 < b_1 else b_1):
            w_1 += self.num_ways(n_1 - 1, v_1 - i, b_1)
        self.dp_matrix[v_1][n_1] = w_1
        return w_1

    def do_your_stuff(self):
        """
            Interface to process_data
        """
        return str(self.lucky_tickets())


def process_data(filename, my_class):
    """
        Get data from file
    """
    file_1 = open(filename)
    testcases = int(file_1.readline())
    for i in range(testcases):
        line = file_1.readline()
        if line == '':
            return
        line = line.replace("\r", "").replace("\n", "")
        args = line.split(' ')
        n_object = my_class(*args)
        print "Line ", i+2, " ", n_object.do_your_stuff()


process_data("DATA.lst", Ticket)
# >python wgiraldom1.py
# Line  2   9858827562998228784995004925
