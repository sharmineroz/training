/*
Linting with CppCheck assuming the #include files are on the same
folder as ciberstein.cpp
$ cppcheck --enable=all --inconclusive --std=c++14 ciberstein.cpp
Checking ciberstein.cpp ...

Compiling and linking using the "Dev-C++ 5.11"

$ g++.exe "C:\\...\ciberstein.cpp" -o "C:\\...\ciberstein.exe"  -I
  "C:\\...\include" -I"C:\\...\include" -I"C:\\...\include" -I"C:\\...\c++" -L
  "C:\\...\lib" -L"C:\\...\lib" -static-libgcc
*/
#include <fstream>
#include <iostream>

using namespace std;

ifstream fin("DATA.lst");

int point(int NV, float *VX, float *VY, float TX, float TY) {
  int i = 0, j = NV-1, c = 0;
  for(;i < NV;j = i++) {
    if((VY[i]>TY)!=(VY[j]>TY)&&TX<(VX[j]-VX[i])*(TY-VY[i])/(VY[j]-VY[i])+VX[i])
      c = !c;
  }
  return c;
}

int main() {

  if(fin.fail())
    cout<<"Error 'DATA.lst' not found";

  else {
    int n,b,c;
    float x,y;
    float a[2][50]={0};

    fin>>n;
    cout<<"answer:"<<endl;

    for(b=0;b<n;b++)
      fin>>a[0][b]>>a[1][b];

    fin>>b;

    for(c=1;c<b+1;c++) {
      fin>>x>>y;

    if(point(n,a[0],a[1],x,y))
      cout<<c<<" ";
    }
  }
  return 0;
}
/*
$ ./ciberstein.exe
3 10 17 19 20 21 23 34 35 36 41 48 55 64 65 77
*/
