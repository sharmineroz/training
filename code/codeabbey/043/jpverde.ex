# $ mix credo
# Analysis took 0.07 seconds (0.03s to load, 0.04s running checks)
# 1 mods/funs, found no issues.
# $ iex
# iex(1)> c("jpverde.ex")

defmodule Dice do
  def data do
    {:ok, contents} = File.read("DATA.lst")
    nums = contents |> String.split([" ", "\r\n"], trim: true)
    [_| vals] = nums
    dat = vals |> Enum.map(&String.to_float/1)

    for val <- dat do
      if val * 6 |> Float.round == 0.0 do
        val = 1.0
        |> Kernel.trunc
      else
        val * 6
        |> Float.round
        |> Kernel.trunc
      end
    end
  end

  def roll do
    roll = Dice.data
    IO.puts([Enum.join(roll, " ")])
  end
end

# iex(8)> Dice.roll
# 6 3 1 5 6 5 4 1 4 3 3 6 3 3 1 5 5 2 4 4 3 4 4 2
# :ok
