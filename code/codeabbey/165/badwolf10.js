/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

// Unnecesary or conflicting rules
/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable no-console */
/* eslint-disable array-bracket-newline */
/* eslint-disable array-element-newline */

const moonradius = 1737100;
const surfgravity = 1.622;
const Vexhaust = 2800;
const deltaT = 0.01;
const burntimechange = 10;
const tco = require('tco');

const simulateSteps = tco((craftparams, burnrate, nsteps) => {
  if ((nsteps === 0) || (craftparams[2] <= 0) || (craftparams[1] <= 0)) {
    // eslint-disable-next-line fp/no-nil
    return [ null, craftparams ];
  }
  const [ craftmass, fuelmass, height, velocity ] = craftparams;
  const nheight = height - (velocity * deltaT);
  const deltaM = burnrate * deltaT;
  const deltaV = Vexhaust * deltaM / (craftmass + fuelmass);
  const nfuelmass = fuelmass - deltaM;
  const currgravity = surfgravity * (Math.pow(moonradius, 2) /
    Math.pow(moonradius + nheight, 2));
  const nvelocity = velocity + (currgravity * deltaT) - deltaV;
  return [ simulateSteps,
    [ [ craftmass, nfuelmass, nheight, nvelocity ], burnrate, nsteps - 1 ],
  ];
});

function simulateLand(craftparams, burnrates) {
  if (burnrates.length === 0) {
    // no more change of fuel rate (Free fall)
    return simulateSteps([ craftparams[0], 1, craftparams[2],
      craftparams[1 + 1 + 1] ], 0, 0 - 1)[1 + 1 + 1];
  }
  if (craftparams[2] <= 0) {
    // Landed
    return craftparams[1 + 1 + 1];
  }
  if (craftparams[1] <= 0) {
    // Out of fuel (Free fall)
    return simulateSteps([ craftparams[0], 1, craftparams[2],
      craftparams[1 + 1 + 1] ], 0, 0 - 1)[1 + 1 + 1];
  }
  const ncraftparams = simulateSteps(craftparams, burnrates[0],
    Math.round(burntimechange / deltaT));
  return simulateLand(ncraftparams, burnrates.slice(1));
}

function getLandSpeed(initparams, burnrates) {
  return simulateLand(initparams, burnrates, 0);
}

function safeLanding(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const initparams = dataLines.slice(0, 1)[0].split(' ').map(Number);
  const burnrates = dataLines.slice(1)[0].split(' ').map(Number);
  console.log(Math.round(getLandSpeed(initparams, burnrates)));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    safeLanding(readerr, contents));
}

main();

/*
$ node badwolf10.js
273
*/
