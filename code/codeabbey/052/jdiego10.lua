--[[
$ luacheck jdiego10.lua
Checking  jdiego10.lua                             OK
Total: 0 warnings / 0 errors in 1 file
]]

local data =io.lines("DATA.lst")
local x=-1

for line in data do
    if x ~= -1 then
     local a
     local b
     local c
     for token in string.gmatch(line, "[^%s]+") do
         a=tonumber(token)
         b=tonumber(token)
         c=tonumber(token)
         if ((a^2) + (b^2)) == (c^2) then
             print("R");
         elseif ((a^2) + (b^2)) < (c^2) then
             print("O");
         else
             print("A");
           end
      end
  end
end
--[[
$ lua jdiego.lua
O A A R R R R R O O A A O A R R R R O A
]]

