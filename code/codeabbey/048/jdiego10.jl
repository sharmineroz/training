#=
 $julia
 julia> using Lint
 julia> lintfile("jdiego10.jl")
=#

open("DATA.lst") do file
  flag = false
  for ln in eachline(file)
     p=0
     b=0
     if flag == false
         flag = true
         continue
      end
      p=parse(Int64)
      b=parse(Int64)
      if p%2 ==0
         p=p/2
         b=b+1
         for i = 0, case-1
             print(b[i])
             end
      elseif p%2 ~=0
         p=3*p+1
         b=b+1
         for i = 0, case-1 do
             print(b[i])
           end
       end
    end
end

#=
 $julia jdiego10.jl
 106 9 24 44 11 104 22 73 64 29 10 26 45 40 20 111 187 126 23 70 11 45 21
=#
