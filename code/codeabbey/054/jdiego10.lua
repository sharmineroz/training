--[[
$ luacheck jdiego10.lua
Checking  jdiego10.lua                             OK
Total: 0 warnings / 0 errors in 1 file
]]

local data =io.lines("DATA.lst")
local x=-1

for line in data do
    if x ~= -1 then
        for token in string.gmatch(line, "[^%s]+") do
            local s = tonumber(token)
            local s2 = math.sqrt(s)
            for m = 1, s2 do
             local found = false
             for n = m+1, s2 do
                 local a, b, c = n*n - m*m, 2*n*m, n*n + m*m
                 local abc = a + b + c
                 if abc == s then
                     io.write(string.format('%d ', c*c))
                     found = true
                 elseif abc > s then
                     break
                   end
                 if found then
                     break
                    end
                end
           end
        end
    end
end

--[[
$ lua jdiego.lua
88390026150409
41051455793881
42074487655225
107016728422321
100589526301489
65692078654225
31933179393025
]]
