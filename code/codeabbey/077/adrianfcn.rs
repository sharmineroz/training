/*
$ rustfmt --check adrianfcn.rs

$ rustc adrianfcn.rs --test -o test
$ ./test
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
$ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
  let mut file = File::open("DATA.lst")?;
  let mut contents = String::new();
  file.read_to_string(&mut contents)?;
  let mut lines = contents.lines();
  let mut points = vec![];
  lines.next();
  for line in lines {
    for n in line.split_whitespace() {
      points.push(n.trim().parse::<f64>().unwrap());
    }
    let mut _dividend = (points[4] - points[0]) * (points[2] - points[0])
      + (points[5] - points[1]) * (points[3] - points[1]);
    let mut _divisor =
      (points[2] - points[0]).powf(2.0) + (points[3] - points[1]).powf(2.0);
    let mut _u = _dividend / _divisor;
    if _u < 1.0 && _u > 0.0 {
      _dividend = (points[2] - points[0]) * (points[5] - points[1])
        - (points[3] - points[1]) * (points[4] - points[0]);
      _divisor = ((points[2] - points[0]).powf(2.0)
        + (points[3] - points[1]).powf(2.0))
      .sqrt();
      let _distance = (_dividend / _divisor).abs();
      if _distance.fract() > 0.0 {
        print!("{:.11} ", _distance);
      }
      else {
        print!("{} ", _distance);
      }
    }
    else if _u < 0.0 || _u > 1.0 {
      let _distance_to_a = (((points[0] - points[4]).powf(2.0)
        + (points[1] - points[5]).powf(2.0))
      .sqrt())
      .abs();
      let _distance_to_b = (((points[2] - points[4]).powf(2.0)
        + (points[3] - points[5]).powf(2.0))
      .sqrt())
      .abs();
      if _distance_to_b < _distance_to_a {
        if _distance_to_b.fract() > 0.0 {
          print!("{:.11} ", _distance_to_b);
        }
        else {
          print!("{} ", _distance_to_b);
        }
      }
      else {
        if _distance_to_a.fract() > 0.0 {
          print!("{:.11} ", _distance_to_a);
        }
        else {
          print!("{} ", _distance_to_a);
        }
      }
    }
    points.clear();
  }
  Ok(())
}

/*
$ ./adrianfcn
14.14213562373 6.40312423743 9.89949493661 3.60555127546 7.28010988928
9.48683298051 5.28982357953 2.60709092226 9.89949493661 12.83487222196
7.29177424715 4.43964044038 3.53553390593 10.29563014099 7.18399304688
16.52605049089 0.65759594922 2.88457184292
*/
