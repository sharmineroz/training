###
$ coffeelint --reporter jslint danmur97.coffee #linting
$ coffee -c danmur97.coffee #compilation
###

english_frequencies = [8.1, 1.5, 2.8, 4.3, 13.0, 2.2, 2.0, 6.1, 7.0, 0.15,
0.77, 7.0, 2.4, 6.8, 7.5, 1.9, 0.095, 6.0, 6.3, 9.1, 2.8, 0.98, 2.4, 0.15,
2.0, 0.074]

count_letters = (word)->
  count={}
  for i in [0...word.length]
    if(!count[word[i]])
      count[word[i]]=1
    else
      count[word[i]]++
  count

element_wise_op = (arr,op,arr2)->
  r=[]
  switch op
    when "+"
      for e in [0...arr.length]
        r[e] = arr[e] + arr2[e]
    when "-"
      for e in [0...arr.length]
        r[e] = arr[e] - arr2[e]
    when "*"
      for e in [0...arr.length]
        r[e] = arr[e] * arr2[e]
    when "/"
      for e in [0...arr.length]
        r[e] = arr[e] / arr2[e]
  r

letters_frequency = (text)->
  text = text.replace(/\s/g, '')
  net_length = text.length
  frecs = {}
  Lnum = count_letters(text)
  for letter,count of Lnum
    frecs[letter] = Math.round(count/net_length*1000)/10
  frecs

avg = (arr)->
  sum = 0
  for i in [0...arr.length]
    sum += arr[i]
  sum/arr.length

unique_letters = (text)->
  r = count_letters(text)
  delete r[" "]
  r

to_string = (x)->
  str = ""
  for i in [0...x.length]
    str = str + x[i] + " "
  str

frequency_diff = (real_frec)->
  diff = element_wise_op(real_frec,'-',english_frequencies)
  avg(element_wise_op(diff,'*',diff))

normalize = (frecs)->
  n = []
  for i in [65..90]
    l = String.fromCharCode(i)
    if frecs[l] != undefined
      n[i-65] = frecs[l]
    else
      n[i-65] = 0

rotate = (arr)->
  tmp=arr[0]
  arr.push(tmp)
  arr.shift()
  arr

caesar_break_key = (text)->
  real_frec = normalize(letters_frequency(text))
  diffs = []
  min = 0
  for i in [0...25]
    real_frec = rotate(real_frec)
    diffs[i] = frequency_diff(real_frec)
    if diffs[i] < diffs[min]
      min = i
  min+1

to_range = (num,min,max)->
  x = num
  if x < min
    x = max+1-(min-x)
  else if x > max
    x = x-max+min-1
  x

caesar_inverse = (text,key)->
  r = ''
  for i in [0...text.length]
    if text[i] != ' '
      r += String.fromCharCode(to_range(text.charCodeAt(i)+key,65,90))
    else
      r += ' '
  r

main = (x)->
  data = x[2]
  data = data.split "\r\n"
  lines = data[0]
  s = ''
  for i in [1..lines]
    key = caesar_break_key(data[i])
    unencrypted = caesar_inverse(data[i],-key)
    words = unencrypted.split " "
    s += words[0]+' '+words[1]+' '+words[2]+' '+key+' '
  console.log(s)
  0

main(process.argv)

###
$ coffee danmur97.coffee "$(< DATA.lst)"
POETRY IS WHAT 5 CARTHAGE MUST BE 22 NO SOONER SPOKEN 21 IT IS BLACK 6
THAT ALL MEN 18
###
