/*
$ eslint neds.js
$ node neds.js
*/

const fileReader = require('fs');

const filePath = 'DATA.lst';
const FIXANGLE = 90;
const DEGREEPI = 180;

function measureHeight(info) {
  let out = '';
  for (let i = 1; i < info.length - 1; i += 2) {
    const distance = info[i];
    const angleB = info[i + 1];
    const angleA = angleB - FIXANGLE;
    let height = distance * Math.tan(angleA * Math.PI / DEGREEPI);
    height = Math.round(height);
    out += height.toString();
    out += ' ';
  }
  process.stdout.write(out);
}

function readData(dat) {
  const info = [];
  const lines = dat.split('\n');
  lines.map((line) => {
    const element = line.split(' ');
    element.map((ele) => info.push(parseFloat(ele)));
    return info;
  });
  measureHeight(info);
}

function main() {
  return fileReader.readFile(filePath, 'utf8', (iss, dat) => {
    if (iss) {
      return iss;
    }
    return readData(dat);
  });
}

main();

/*
$ node neds.js
20 64 34 43 25 54 55 42 20 36 50 25 61 16
*/
