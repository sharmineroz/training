/*
$ rustfmt adrianfcn.rs
$ rustc --test adrianfcn.rs
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
$ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;
use std::f64::consts::PI;

fn tree_height_measurement(d : i64, b : f64) {
  let mut a = - (90.0 - b);
  a = a * PI/180.0;
  let h = d as f64 * a.tan();
  print!("{} " , h.round());
}

fn main() -> std::io::Result<()> {
  let mut file = File::open("DATA.lst")?;
  let mut contents = String::new();
  file.read_to_string(&mut contents)?;
  let mut lines = contents.lines();
  lines.next();
  for l in lines {
    let mut param1 : i64 = 0;
    let mut param2 : f64 = 0.0;
    for c in l.split_whitespace() {
      if param1 == 0 {
        param1 = c.trim().parse::<i64>().ok().unwrap();
      } else {
        param2 = c.trim().parse::<f64>().ok().unwrap();
      }
    }
    tree_height_measurement(param1, param2);
  }
  println!();
  Ok(())
}
/*
$ ./adrianfcn
  20 64 34 43 25 54 55 42 20 36 50 25 61 16
*/
