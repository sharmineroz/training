#!/bin/bash
#
# Problem #5 Minimum of Three
#
while read -r line || [[ -n "$line" ]]; do
    len=$(echo -n "$line" |  wc -c)
    if [[ "$len" -gt 2 ]]
    then
        count=1
        data_array=($line)
        if [[ "${data_array[0]}" -lt "${data_array[1]}" ]] &&\
                   [[ "${data_array[0]}" -lt "${data_array[2]}" ]]

        then 
            output_array[$count]="${data_array[0]}"

        elif [[ "${data_array[1]}" -lt "${data_array[0]}" ]] &&\
            [[ "${data_array[1]}" -lt "${data_array[2]}" ]]
        then
            output_array[$count]="${data_array[1]}"

        elif [[ "${data_array[2]}" -lt "${data_array[0]}" ]] &&\
            [[ "${data_array[2]}" -lt "${data_array[1]}" ]]
        then
            output_array[$count]="${data_array[2]}"
        fi
        #output_array[$count]="$min_value"
        let "count+=1"
    fi
    printf '%s ' "${output_array[@]}"

done < "$1"
