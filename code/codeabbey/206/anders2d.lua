--[[
$ ./luacheck.exe anders2d.lua
Checking anders2d.lua               OK
Total: 0 warnings / 0 errors in 1 file
]]
local ABCD =   {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P",
       "Q","R","S","T","U","V","W","X","Y","Z","2","3","4","5","6","7"}

local ABCD2 = {A = 0,B = 1,C = 2 ,D = 3 ,E = 4,F = 5,G = 6,H = 7,
    I = 8,J = 9,K = 10,L = 11, M = 12,N = 13,O = 14,
    P= 15,Q = 16,R = 17,S = 18 ,T = 19,U = 20,V = 21,
    W = 22,X = 23,Y= 24,Z = 25 }

ABCD2[2] = 26
ABCD2[3] = 27
ABCD2[4] = 28
ABCD2[5] = 29
ABCD2[6] = 30
ABCD2[7] = 31

require 'io'

local data
local testCases ={}
local size

data = io.lines("DATA.lst")

local function arrayToStr(delimiter, list)
  local len = #list
  if len == 0 then
    return ""
  end
  local string = list[1]
  for i = 2, len do
    string = string .. delimiter .. list[i]
  end
  return string
end

local function strToArray(str)
  local tmpArray={}
  str:gsub(".",function(c) table.insert(tmpArray,c) end)
  return tmpArray
end

local function toBits(num,bits)
  bits = bits or math.max(1, select(2, math.frexp(num)))
  local t = {} -- will contain the bits
  for b = bits, 1, -1 do
    t[b] = math.fmod(num, 2)
    num = math.floor((num - t[b]) / 2)
  end
  return t
end

local function arrayToBinary(array,bits)
  local tmpArray = {}
  for i = 1, #array do
    table.insert(tmpArray,toBits(string.byte(array[i]),bits))
  end
  return tmpArray
end

local function splitArrayInParts(array,nParts)
  local tmpMod = 0
  local tmpArrayFive = {}
  local newArraySplitted = {}

  for i = 1 , #array do
    for j = 1 , #array[i] do
      tmpMod=tmpMod%nParts
      table.insert(tmpArrayFive,array[i][j])

      if(tmpMod==nParts-1) then
        table.insert(newArraySplitted,tmpArrayFive)
        tmpArrayFive={}
      end
      tmpMod=tmpMod+1
    end
  end
  return newArraySplitted
end

local function  arrayIntToBinary(array,bits)
  local tmpArray = {}
  for i = 1, #array do
    table.insert(tmpArray,toBits(array[i],bits))
  end
  return tmpArray
end

local function StrToBase32(text)
  local textArray = strToArray(text)
  local lenText = #textArray
  local binaryArray
  local textModFive = lenText%5
  local Base32 = {}


  for _= 1 , 5-textModFive do
    table.insert(textArray,(5-textModFive))
  end
  binaryArray = arrayToBinary(textArray,8)
  binaryArray =splitArrayInParts(binaryArray,5)

  for i  = 1, #binaryArray do
    local strBinary = arrayToStr("",binaryArray[i])
    local num = tonumber(strBinary,2) + 1
    local letter = ABCD[num]
    table.insert(Base32,letter)
  end
  return arrayToStr("",Base32)
end

local function base32ToStr(base32)
  local base32Array = strToArray(base32)
  local binaryArray
  local text  = {}
  local tmpBase32 = {}

  for i  = 1, #base32Array do
    local num
    if (tonumber(base32Array[i]) == nil) then
       num = ABCD2[base32Array[i]]
    else
       num = ABCD2[tonumber(base32Array[i])]
    end
    table.insert(tmpBase32,num)
  end

  binaryArray = arrayIntToBinary(tmpBase32,5)
  binaryArray = splitArrayInParts(binaryArray,8)

  for i  = 1, #binaryArray do
    local strBinary = arrayToStr("",binaryArray[i])
    local num = tonumber(strBinary,2)
    local letter = string.char(num)
    if (tonumber(letter)==nil) then
      table.insert(text,letter)
    end
  end
  return arrayToStr("",text)
end

local function main ()

  for line in data do
    if (size == nil) then
      size = tonumber(line)
    else
      table.insert(testCases,line)
    end
  end
  for i = 1, size do
    if (i%2 == 0 )then
      io.write(base32ToStr(testCases[i]))
    else
      io.write(StrToBase32(testCases[i]))
    end
    io.write(" ")
  end
end

main()

--[[
$ lua -e "io.stdout:setvbuf 'no'" "anders2d.lua"
NBQW423ZEBRXK4TSPFRW63LCMVSDGMZT roiled unformed overtaxes
MRXXOZLMEBUG65LOMRUW4ZZR lyricist squiggliest ORUHE2LGORUWK43UGU2TKNJV
amateurism radiotelephones blowzy graveling NFWWCZ3JNZTSA4DJNZTGKYLUNBS
XEMRS illustrator appendixes disengagements unobtrusively foolishly
O5QXMZLMMVXGO5DIOMQG22LTMVZGY2LOMVZXGIDUN52WO2DFON2DGMZT audio
MNQW433QPEQHA33TOR2WYYLUMVSCAYLNNZSXG5DZNFXGOIDHOJXWC3TTGU2TKNJV
eruditely ORZGS4DPMQQGS3TUMVXHG2LGPFUW4ZZAONUWIZLXMF4XGIDSMVSGK5TFNRXXAID
LNZQXA43BMNVTGMZT skated supplement sleazy NRXWOIDROVQXG2BR miscellany
unpopular overheard terrified NZXW43TFM5XXI2LBMJWGKIDJNVYG64TUMF2GS33OO
MQGM5LTNFRGYZJR lament adolescent teaspoonfuls cistern MRZGS3TLOM2DINBU
flowerpots computing ONUWO3TJMZUWGYLOOQ2DINBU successive eke intermarry
potholders churchman ORUG65LHNB2HGIDQN5WHS3LFOJUWGMRS
]]
