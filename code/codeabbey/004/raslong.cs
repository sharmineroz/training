﻿using System;

namespace ConsoleChallange
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            String [,] val=new String[n, 2];
            String Line;
            String[] lspl;
            for (int i = 0; i < n; i++)
            {
                Line = Console.ReadLine();
                lspl = Line.Split(' ');
                for (int j = 0; j < 2; j++)
                {
                    val[i, j] = lspl[j];
                }
            }
            Console.WriteLine(" ");
            for (int i = 0; i < n; i++)
            {
                if (Int64.Parse(val[i,0])> Int64.Parse(val[i,1]))
                {
                    Console.Write(val[i, 1]+" ");
                }
                else
                {
                    Console.Write(val[i, 0] + " ");
                }
            }
            Console.ReadKey(true);
        }
    }
}
