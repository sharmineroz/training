#=
 $julia
 julia> using Lint
 julia> lintfile("kergrau.jl")
=#

open("DATA.lst") do file
  flag = false
  answer = ""

  for ln in eachline(file)
    if flag == false
      flag = true
      continue
    end

    i = split(ln, " ")
    number1 = parse(Int64, i[1])
    number2 = parse(Int64, i[2])

    if number1 > number2
      answer = "$number2 "
    else
      answer = "$number1 "
    end

    println(answer)
  end
end

# $julia kergrau.jl
# -2605178 -8641437 -1673153 -8560512 391090 -6006084 -446852 -7897953
# 5695945 6727636 -6232151 320213 -8684553 -7369740 -4312186 3082635 -9672707
# -7403785 -1345861 -9665427 -8571476 -5498134 1971156 3278711 1395820
# 1187365 -6111861 -4366784
