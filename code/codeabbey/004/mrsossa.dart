/**
 * $ dartanalyzer mrsossa.dart
 * Analyzing mrsossa.dart
 * No issues found!
 */

import 'dart:io';

void main() {
  new File('DATA.lst').readAsLines().then((var data) {
    var c = int.parse(data[0]);
    var n1,n2;

    for (var i = 1; i <= c; i++) {
      var cas = data[i];
      cas.split(" ");
      n1 = int.parse(cas[1]);
      n2 = int.parse(cas[2]);

      if (n1<n2) {
        print(n1);
      }
      else {
        print(n2);
      }
    }
  }
  );
}

/* $ dart mrsossa.dart
 * 2 6 1 4 6 0 1 2 6 2 2 0 4 2 3 6 0 5 1 5 5 4 5 6 4 1 4
*/
