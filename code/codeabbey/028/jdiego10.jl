#=
 $julia
 julia> using Lint
 julia> lintfile("jdiego10.jl")
=#

open("DATA.lst") do file
  flag = false
  for ln in eachline(file)
      p=0
      a=0
      m=o
     if flag == false
         flag = true
         continue
      end
     p=parse(Int64)
     a=parse(Int64)
     m=(p/a)^2
     if m<18.5
         printl("under")
      end
     if m<25.0 && m>=18.5 then
         printl("normal")
      end
     if m<30.0 && m>=25.0 then
         printl("over")
      end
     if m>=30.0 then
         printl("obese")
     end
   end
end

#=
$julia jdiego10.jl
over normal under obese obese obese obese over under normal normal under
normal normal obese normal obese under normal obese obese normal obese under
obese under under obese obese obese under obese normal normal under
=#
