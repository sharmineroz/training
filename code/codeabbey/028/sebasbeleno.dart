/**
 * $ dartanalyzer sebasbeleno.dart
 * Analyzing sebasleno.dart
 * No issues found!
 */
import 'dart:io';
import 'dart:math';
import 'package:path/path.dart' show dirname, join;

void main() {
  var data = File(join(dirname(Platform.script.path), 'DATA.lst'));

  data.readAsLines().then((List data) {
    var line = data[0];
    var testCases = int.parse(line);
    var answers = [];

    for (var i = 1; i <= testCases; i++) {
      var strWeightAndHeight = data[i];
      var weightAndHeight = [];
      weightAndHeight = strWeightAndHeight.toString().split(' ');
      var weight = int.parse(weightAndHeight[0]);
      var height = double.parse(weightAndHeight[1]);
      var BMI = getBMI(height, weight);

      if (BMI < 18.5) {
        answers.add('under');
      } else if (BMI >= 18.5 && BMI < 25.0) {
        answers.add('normal');
      } else if (BMI >= 25.0 && BMI < 30.0) {
        answers.add('over');
      } else {
        answers.add('obese');
      }
    }
    for (var answer in answers) {
      print(answer);
    }
  });
}

double getBMI(height, weight) {
  var BMI = weight / pow(height, 2);
  return BMI;
}

/*$ dart sebasbeleno.dart
  * over normal under obese obese obese obese over under normal
  normal under normal normal obese normal obese under normal obese obese
  normal obese under obese under under obese obese obese under obese normal
  normal under
*/
