open System

[<EntryPoint>]
let main argv =
    let operations (w : float) (h : float) =
        let r = "under"
        let bmi =  w/(h*h)
        if (bmi <= 18.5) then (r)
        elif ((18.5 <= bmi) & (bmi < 25.0)) then "normal"
        elif ((25.0 <= bmi) & (bmi < 30.0)) then "over"
        else "obese"
    printfn "cuantas personas se van a pesar?"
    let c = stdin.ReadLine() |> int
    let mutable BMI = [| for i in 0 .. (c-1) -> "" |]
    for i in 0..(c-1) do
        printfn "(%i) ingrese su peso (Kg) y altura (mts) separados por un espacio:" (i+1)
        let dataArray = stdin.ReadLine().Split ' '
        let w = float dataArray.[0]
        let h = float dataArray.[1]
        BMI.[i] <- operations w h
    for i in 0..(c-1) do
        printf "%s " BMI.[i]
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero
