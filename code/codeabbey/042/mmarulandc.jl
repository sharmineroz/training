#=
 $julia
 julia> using Lint
 julia> lintfile("mmarulandc.jl")
=#

f = open("DATA.lst")
lines = readlines(f)
tries = lines[1]
tam = length(lines)
deck = Dict("A" => 11, "2" => 2,"3" => 3,"4" => 4, "5" => 5,"6" => 6,"7" => 7,
"8" => 8,"9" => 9,"T"=>10,"J" => 10,"Q" => 10,"K" => 10)
results = []
aceCounter = 0
for i = 2:tam
  black = 0
  arrayLines = split(lines[i])
  for x in arrayLines
    if x == "A" && black + 11 > 21
      black = black + 1
    else
      black = black + deck[x]
    end
  end
  push!(results,black)
end

for i in results
  if i > 21
    print("Bust"," ")
  else
    print(i, " ")
  end
end
close(f)

# $ julia mmarulandc.jl
# 17 19 17 18 Bust 17 20 Bust 18 Bust 18 21 16 21 20 18 17 18 20 20 16 21 21 21
