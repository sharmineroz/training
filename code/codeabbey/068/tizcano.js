/*
$ eslint tizcano.js
$
*/

function solver(distance, speedA, speedB) {
  return (distance / (speedA + speedB)) * speedA;
}
function solverLink(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n');
  const inputArray = inputFile.slice(1);
  const solvedArray = inputArray.map((element) => {
    const lineArray = element.split(' ').map((ele) => parseInt(ele, 10));
    return solver(lineArray[0], lineArray[1], lineArray[2]);
  });
  const output = process.stdout.write(`${ solvedArray.join(' ') }`);
  return output;
}

const fileReader = require('fs');
function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solverLink(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node tizcano.js
output:
6.681818181818182 27.925925925925927 5.75 64.42307692307693 18.5625
70.10714285714286 11.063829787234043 47.51282051282051 39.76744186046511
9.25925925925926 32.869565217391305 152.4193548387097 163.95555555555555
5.28125 298.2 40.285714285714285 5.7272727272727275 45.68421052631579
15.545454545454547 26.666666666666664 41.26315789473684 12 8.307692307692308
26.511627906976745 12.23404255319149 35.18518518518518 4.333333333333333 117.5
*/
