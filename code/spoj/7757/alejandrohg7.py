#!/usr/bin/env python3
'''
$ pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function


def open_file():
    '''This function open and reads a file '''
    with open('DATA.lst', 'r') as opened_file:
        list_of_lists = []
        for row in opened_file:
            row = row.rstrip("\n")
            list_of_lists.append(row)
        return list_of_lists


def tautograms_evaluator(tautogram):
    '''This function evaluates if a sentence is a tautogram'''
    list_words = tautogram.split(' ')
    tautogram_container = []
    for letter in list_words:
        letter = letter[0].lower()
        tautogram_container.append(letter)
    validator = tautogram_container[0]
    same = True
    for element in tautogram_container:
        if element != validator:
            same = False
            break
    if same:
        result = "Y"
    else:
        result = "N"
    return result


for case in open_file()[:-1]:
    print(tautograms_evaluator(case))

# $ python3 alejandrohg7.py
# Y
# Y
# Y
# Y
# N
