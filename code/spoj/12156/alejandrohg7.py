#!/usr/bin/env python3
'''
$ pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function


def open_file():
    '''This function open and reads a file '''
    with open('DATA.lst', 'r') as opened_file:
        list_of_lists = []
        for number in opened_file:
            list_of_lists.append(number.split())
        return list_of_lists


def half_of_the_half(word_to_half):
    '''This function half a word and every two spaces it half that letter'''
    counter = 0
    list_letters = []
    while counter < (len(word_to_half)//2):
        list_letters.append(word_to_half[counter])
        counter += 2
    list_letters = "".join(list_letters)
    print(list_letters)


for word in open_file():
    word = word[0]
    half_of_the_half(word)

# $ python3 alejandrohg7.py
# y
# po
# i
# ntc
# i
# m
# a
# b
# d
