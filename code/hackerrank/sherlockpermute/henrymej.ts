
/*
tslint --init #linting configuration
tslint henrymej.ts #linting
tsc henrymej.ts #compiling into js
*/
function factorial_begin(num, begining) {
    let i = begining + BigInt(1);
    let prod = BigInt(1);
    while (i <= num) {
        prod = (prod * i) ;
        i = i + BigInt(1);
    }
    return prod;
}

function factorial(num) {
    let i = BigInt(1);
    let prod = BigInt(1);
    while (i <= num) {
        prod = (prod * i) ;
        i = i + BigInt(1);
    }
    return prod;
}

function solve(a, b) {
    if (a >= b - 1) {
      const numerator = factorial_begin(BigInt(a + b - 1), BigInt(a));
      const denominator = factorial(BigInt(b - 1));
      return (numerator / denominator) % BigInt(1000000007);
    }
    const numerator = factorial_begin(BigInt(a + b - 1), BigInt(b - 1));
    const denominator = factorial(BigInt(a));
    return (numerator / denominator) % BigInt(1000000007);
}

const fs = require("fs");
fs.readFile(__dirname + "/DATA.lst", (err, data) => {
    if (err) {
        throw err;
    }
    const inputs = data.toString().split("\n");
    const T = inputs[0];
    let outputs = "";
    for (let index = 1; index <= T; index++) {
        const n = parseInt(inputs[index], 10);
        const m = parseInt(inputs[index], 10);
        outputs = outputs + " " + solve(n, m);
    }
    outputs = outputs.substring(1, outputs.length);
    console.log(outputs);
});

/*
node henrymej.js
20058300 20058300 20058300 20058300 20058300 77558760 77558760 77558760
77558760 77558760 77558760 77558760 77558760 77558760 77558760 77558760
300540195 300540195 300540195 300540195 300540195 300540195 300540195
300540195 300540195 300540195 300540195 166803103 166803103 166803103
166803103 166803103 166803103 166803103 166803103 166803103 166803103
166803103 537567622 537567622 537567622 537567622 537567622 537567622
537567622 537567622 537567622 537567622 537567622 672631781
*/
