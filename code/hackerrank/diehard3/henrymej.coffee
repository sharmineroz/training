
###
coffeelint henrymej.cofee
###

fs = require 'fs'

gcd = (x, y) ->
  while(y)
    m = x % y
    x = y
    y = m
  return x

solve = (a, b, c) ->
  if(c > a && c > b)
    return 'NO'
  if (c % gcd(a,b) == 0)
    return 'YES'
  return 'NO'

hackerrank = ->
  fs.readFileSync 'DATA.lst', 'utf8'

doc = hackerrank().split('\n')
tc = parseInt(doc[0], 10)
i = 1
solution = ''
loop
  a = parseInt(doc[i].split(' ')[0], 10)
  b = parseInt(doc[i].split(' ')[1], 10)
  c = parseInt(doc[i].split(' ')[2], 10)
  solution =  solution.concat(solve a, b, c).concat(' ')
  if i == tc
    break
  i += 1
console.log(solution)

###
coffee henrymej.coffee
YES NO NO YES YES NO NO YES NO NO YES NO YES YES YES YES NO YES NO YES NO
YES NO YES NO NO NO YES NO YES
###
