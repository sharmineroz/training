def pagesNumbering(n):
    d = 0
    p = 1
    while n >= 10 ** p:
        d += p * 9 * 10 ** (p - 1)
        p += 1

    return d + p * (n - 10 ** (p - 1) + 1)
