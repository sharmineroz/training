//Alumnos aprobados y alumnos reprobados

import java.util.Scanner;

public class Reto5{
    public static void main(String[] args) {

        Scanner digit  = new Scanner(System.in);
        int nota       = 0;
        int aprobados  = 0;
        int reprobados = 0;

        System.out.println("");
        System.out.println("*******");
        System.out.println("*Reto5*");
        System.out.println("*******");

        System.out.println("");
        System.out.println("****************************************");
        System.out.println("*Alumnos aprobados y alumnos reprobados*");
        System.out.println("****************************************");
        System.out.println("");

        for (int i =1; i<=10; i++) {
            System.out.print("Ingrese una nota entre 0 y 10 por favor: ");
            nota = digit.nextInt();
            if (nota>=7) {
                aprobados = aprobados + 1;
            }
            else {
                reprobados = reprobados +1;
            }
        }
        System.out.println("");
        System.out.println("Alumnos aprobados es: "+aprobados);      
        System.out.println("Alumnos reprobados es: "+reprobados);  
    } 
}    
