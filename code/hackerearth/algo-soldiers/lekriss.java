/*
$ Checkstyle lekriss.java #linting
$ javac lekriss.java #Compilation
*/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.Math;
import java.util.Collections;

class lekriss {

  public static void main (String args[]) throws Exception{
    File file = new File("DATA.lst");
    FileReader r = new FileReader(file);
    BufferedReader br = new BufferedReader(r);
    Scanner input = new Scanner(br);
    int quantityOfSoldiers = input.nextInt();
    ArrayList<Integer> soldiers = new ArrayList<Integer>();
    for(int i=0; i<quantityOfSoldiers; i++)
      soldiers.add(input.nextInt());

    Collections.sort(soldiers);

    String control = "";
    while(input.hasNext()) {
      int powerToTest = input.nextInt();
      int sumOfPowers = 0;
      int oldSumOfPowers = 0;
      int i = 0;
      String result = "";
      while(soldiers.get(i)<=powerToTest) {
        sumOfPowers += soldiers.get(i);
        i++;
        if(i==quantityOfSoldiers)
          break;
      }
      result = i + " " + sumOfPowers;
      if(!result.equals(control)) {
        System.out.println(result);
        control = result;
      }
    }
  }
}
/*
$ java lekriss
$ 3 6
$ 7 28
$ 2 3
*/
