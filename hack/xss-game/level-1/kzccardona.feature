## Version 1.0
## language: en

Feature: Cross-site scripting on user input
  Site:
    Xss-game
  Category:
    Cross-site scripting
  User:
    kzccardona
  Goal:
    Interact with the vulnerable application and find a way to
    Make it execute JavaScript of your choosing

  Background:
  Hacker's software:
    |<version>             | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Google Chrome        | 75.0  (64 bit) |
  Machine information:

  Scenario: fail: Using element inspector
    Given I read the problem
    When I open an element inspector
    Then I try to write the following javascript code:
    """
      <script>
          $(document).ready(function(){
            alert('hello');});
      </script>
    """
    When I try to execute the code
    Then it didn't work
    And I couldn't capture the flag

  Scenario: Success: Using user input
    Given I read the problem
    When I read about XSS in web
    And I find how to inject javascript in a diferents ways
    Then I try writing in the input: "<script>alert('hello')</script>"
    When I press the send button
    Then I get an alert with my message
    And the page give me the flag:
    """
      Congratulations, you executed an alert:
        Hello
      You can now advance to the next level
    """
