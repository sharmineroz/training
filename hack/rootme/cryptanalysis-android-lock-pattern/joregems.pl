#!/usr/bin/perl
use String::HexConvert ':all';
use Digest::SHA1  qw(sha1 sha1_hex sha1_base64);
use warnings;
use strict;
###########BEGIN_BODY###############################
main();
#########################END_BODY####################

sub main{
  my $key=get_key();
  my @my_set=("\x00","\x01","\x02","\x03","\x04","\x05","\x06","\x07","\x08");
  #define the set for the initial permutations
  my @permutations=brute(@my_set); #get all permutations
  print "the password in hexadecimal is:  ".get_password($key,@permutations);
  print "\n"; #get the password
}

sub get_password{#go trough the dictionary checking coincidences
  my ($key_,@chains_)=@_;
  for (my $i=0;$i<9;$i++){
    my @arr = reduction($i,@chains_);#finish to create the dictionary adding
    #the combinations that are substrings from the original permutations
    foreach my $item(@arr){
      my $digest = sha1_hex($item);
      if($digest eq $key_){
        return unpack("H*",$item);#convert the result in a readable format
      }
    }
  }
}

sub get_key{#get the file with the key normally gesture.key
my $gesture_key;
my $data = $ARGV[0]; # the path for the gesture key we want decrypt
  open(FH, '<', $data) or die $!; #open the gesture.key file
  while(<FH>){ #go through the file line by line
    $gesture_key= ascii_to_hex($_); #assigning the value to the variable
    last;# the gesture.key file should just have one line
  }
  close FH;
  return $gesture_key;
}

sub brute{
  my @domain=@_;
  my @result;
  @result=generate(scalar @domain, \@domain);
  return @result;
}

sub reduction{#this is for trim the strings in n elements to make new
#elements substrings from the original permutations
  my ($n,@array)=@_;
  my @shortChains;
  if ($n==0){#here is nothing to trim
  return @array;
  }
  else{
      foreach my $item(@array) {
      my $short =substr( $item, 0, length($item)-$n*4);
      push @shortChains, $short;
    }
  }
  my %hash = map { $_ => 1 } @shortChains;
  #create a hash with keys for eliminate repeated values
  my @unique = keys %hash; #retrieve the keys in to an array
  return @unique;
}

############BEGIN_GENERATE########################
sub generate {
 #heap algorithm written from
#https://es.wikipedia.org/wiki/Algoritmo_de_Heap

  my ($n, $refA) = @_;
  my @A=@{$refA};
  my @c;
  my $rec;
  my @result;
  for (my $i=0;$i<$n;$i++){
    @c[$i]=0;
  }
  $rec = join('',@A);
  push @result, $rec;

  my $i=0;
  while($i<$n){
    if ($c[$i]<$i){
      if($i%2==0){
        @A[0,$i]=@A[$i,0];
      }
      else{
        @A[$c[$i],$i]=@A[$i,$c[$i]];
      }
      $rec = join('',@A);
      push @result, $rec;
      $c[$i] += 1;
      $i = 0;
    }
    else{
      @c[$i]=0;
      $i++;
    }
  }
  return @result;
}
###################END_GENERATE############
