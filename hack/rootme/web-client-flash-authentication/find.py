"""
    Script for imitating the ActionScript algorithm
    > pylint find.py.spaghetticode
    ************* Module find.py
    find.py.spaghetticode:16:4: C0200: Consider using enumerate
    instead of iterating with range and len (consider-using-enumerate)
    -------------------------------------------------------------------
    Your code has been rated at 9.29/10 (previous run: 9.31/10, +0.69)
"""


def write_file():
    """
        Create another file with the converted bytes
    """
    file_1 = open("1_RootMe_EmbeddedSWF.bin", 'rb')
    key = b'rootmeifyoucan'
    out = b''
    loc = 0
    file_bytes = file_1.read()
    for i in file_bytes:
        out += bytes([i ^ key[loc]])
        loc += 1
        if loc >= len(key):
            loc = 0
    f_out = open("Out.swf", "wb")
    f_out.write(out)


write_file()
