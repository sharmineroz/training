# -*- coding: utf-8 -*-
# pylint keyloguer.py
# No config file found, using default configuration
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
Code to solve the challenge homemade-keylogger from rootme
"""

import struct


def parse_events():
    """
    Method to convert the binary values of the events.bin to text
    """

    infile_path = "events.bin"
    # long int, long int, unsigned short, unsigned short, unsigned int
    struct_format = 'llHHI'
    # size of the struct
    event_size = struct.calcsize(struct_format)

    # output file
    output_file = open("keylogger.txt", "w")

    # open file in binary mode
    in_file = open(infile_path, "rb")
    # read from file
    event = in_file.read(event_size)
    # while event
    while event:
        # unpack binary value to each of the values
        (tv_sec, tv_usec, key_type, code, value) = \
            struct.unpack(struct_format, event)
        # print only valid events
        if key_type != 0 or code != 0 or value != 0:
            # save to file
            output_file.write("Event type %u, code %u, value %u at %d.%d\n" %
                              (key_type, code, value, tv_sec, tv_usec))
        # read next struct
        event = in_file.read(event_size)
    # close file
    in_file.close()
    output_file.close()


# convert events.bin to text file
parse_events()

# read file with events as text
F = open("keylogger.txt")

# keymap
K = {'1': 'esc', '2': 'c', '3': 'é', '4': '\"', '5': '\'', '6': '(', '7': '-',
     '8': 'è', '9': '_', '10': 'ç', '11': 'à', '12': ')', '13': '=',
     '14': 'delete', '15': '    ', '16': 'a', '17': 'z', '18': 'e', '19': 'r',
     '20': 't', '21': 'y', '22': 'u', '23': 'i', '24': 'o', '25': 'p',
     '26': '^', '27': '$', '28': 'enter', '29': 'ctrl', '30': 'q', '31': 's',
     '32': 'd', '33': 'f', '34': 'g', '35': 'h', '36': 'j', '37': 'k',
     '38': 'l', '39': 'm', '40': 'ù', '41': 'none', '42': 'shift', '43': '*',
     '44': 'w', '45': 'x', '46': 'c', '47': 'v', '48': 'b', '49': 'n',
     '50': ',', '51': ';', '52': ':', '53': '!', '54': 'shift', '55': '*',
     '56': 'alt', '57': ' ', '58': 'none', '59': 'f1', '60': 'f2', '61': 'f3',
     '62': 'f4', '63': 'f5', '64': 'f6', '65': 'f7', '66': 'f8', '67': 'f9',
     '68': 'f10', '69': 'none', '70': 'bloq_desktop', '71': '7', '72': '8',
     '73': '9', '74': '-', '75': '4', '76': '5', '77': '6', '78': '+',
     '79': '1', '80': '2', '81': '3', '82': '0', '83': '.', '86': '<',
     '184': 'altgr', '156': 'enter', '200': 'up\n'}

# array to store the type of event and the keyboard
EVENT_DICTIONARY = []

# var to exit when there are not more lines
END = 0

# iterate over file
while not END:
    # read first line containig key
    LINE = F.readline()
    # read second line containing type
    LINE2 = F.readline()
    # exit when no more lines
    if not LINE:
        END = 1
    # else insert into dictionary
    else:
        CURRENT_DIC = {'key': LINE.split(' ')[6],
                       'type': int(LINE2.split(' ')[6])}
        EVENT_DICTIONARY.append(CURRENT_DIC)
# array to store when key is down type = 1
ARRAY_KEYS = []
# string to store the output
SOL = ''
# count keys
i = 0

# iterate over events
for d in EVENT_DICTIONARY:
    # get the value of the key
    key = d["key"]
    # check if type is 1, key down
    if d["type"] == 1:
        # add to array waiting for release
        ARRAY_KEYS.append(key)
        if K[key] == "shift" or K[key] == "altgr":
            # if key == shift or altgr concat <shift> or <altgr>
            # all between <shift> and </shift> was with shift key pressed
            SOL += "<"+K[key]+">"
    # check if type = 0 key up
    elif d["type"] == 0 and i != 0:
        # if key == shift or altgr concat </shift> or </altgr
        if K[key] == "shift" or K[key] == "altgr":
            SOL += "</"+K[key]+">"
        # if key = delete then delete last char
        elif K[key] == "delete":
            SOL = SOL[:-1]
        # finally by default concat to output string
        else:
            SOL += K[key]
        # remove from down array because key was released
        ARRAY_KEYS.remove(key)
    i += 1

# some replacements to understand better the output
SOL = SOL.replace("enter", "\n")
SOL = SOL.replace("<altgr>)</altgr>", "]")
SOL = SOL.replace("<altgr>(</altgr>", "[")
SOL = SOL.replace("<altgr></altgr>", "")
SOL = SOL.replace("<altgr>-</altgr>", "|")
SOL = SOL.replace("<shift>;</shift>", ".")
SOL = SOL.replace("<shift>=</shift>", "+")
SOL = SOL.replace("<shift><</shift>", ">")
SOL = SOL.replace("<shift></shift>:", "/")
SOL = SOL.replace("<shift>;:</shift>", "./")
SOL = SOL.replace("<shift>:</shift>", "/")
SOL = SOL.replace("<shift></shift>ù", "%")
SOL = SOL.replace("<altgr>_</altgr>", "\\")
SOL = SOL.replace("<shift>ù</shift>", "%")
SOL = SOL.replace("<altgr>\"</altgr>", "#")
SOL = SOL.replace("<shift></shift>;", ".")
SOL = SOL.replace("<shift>:;</shift>", "/.")
SOL = SOL.replace("<altgr>é</altgr>", "~")
SOL = SOL.replace("<altgr>à</altgr>", "@")
SOL = SOL.replace("<shift>u</shift>", "U")
SOL = SOL.replace("<shift>x</shift>", "X")
SOL = SOL.replace("<shift>g</shift>", "G")
SOL = SOL.replace("^^", "^")
# print the final output
print SOL
