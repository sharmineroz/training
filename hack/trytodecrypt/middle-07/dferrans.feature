## Version 2.0
## language: en

Feature: middle-07-decrypt-trytodecrypt.com #M

  Code:
    middle-07
  Site:
    trytodecrypt.com
  Category:
    decrypt
  User:
    dferrans
  Goal:
    decrypt secret string

  Background:

  Hacker's software:
    | <Software name> | <Version>   |
    | macos           | 10.14.5     |
    | chrome          | 74.0.3729   |

  Machine information:
    Given the challenge URL
    """
    https://www.trytodecrypt.com/decrypt.php?id=7&solution=1#headline
    """

  Scenario: Success:build-dictionary-to-decrypt-secret-word

    Given I am accessing the challenge through a webbrowser
    And I have the desired string to decrypt
    Then I can get each all values to build dictionary
    Then I try to decrypt the string by doing Creating a script
    And get the string that solves the hack: this was confusing
