## Version 2.0
## language: en

Feature: 5-Hacking-w3challs
  Site:
    w3challs
  User:
    jpverde
  Goal:
    Access the website

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0        |
  Machine information:
    Given A link for a webpage
    """
    browser.hacking.w3challs.com/index.php
    """
    And A web page that only allows access if you have W3_challs browser
    And The webpage says
    """
    To solve this challenge, your browser must be: W3Challs_browser
    """

  Scenario: Success:change-user-agent
    Given I am in the webpage
    When I read the message
    """
    Your current browser is Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0)
    Gecko/20100101 Firefox/68.0
    """
    Then I realize it is talking about User agent
    And I go to the "about:config" section of my browser
    And I create a new section called "general.useragent.override"
    And I modify the value to W3Challs_browser
    And I save changes
    And I reload the page
    When The webpage shows this text
    """
    Well done ! You solved this challenge, the password to validate it is
    the acronym of "SQL"
    ( Everything lowercase with spaces between each word )
    """
    Then I return to the challenge section
    And I write on the password field "structured query language"
    And I solved the challenge
