# language: en

Feature: Solve challenge 27
  From site W3Challs
  From Cryptography Category
  With my username Skhorn

  Background:
    Given the fact i have an account on W3Challs site
    And i have Debian 9 as Operating System
    And i have internet access

  Scenario: Failed attempt
    Given the link to the challenge
    And a description
    And a set of indications 
    And the indications states a crypto function f(x)=21x+11[26]
    And it says i have to encrypt a word with it
    And i code a script to help me with this task
    And i code another function to convert the characters into numbers
    And i use ascii for this task
    And i have to decrypt the output of the ciphertext created before
    And the indications states a decrypt function g(y)=ay+b[26]
    And i code a function for this task
    And by this i might find the decryption function
    And by knowing that y=f(x)
    And by kwoning that f(x) stands for the ciphertext
    And i code a function to find the decryption values a, b
    And by this i might find the decryption function
    And i code a function to decrypt it
    And i use the function to convert the characters into numbers
    And i see the range of the numbers given 
    And i see it is too low to print a readable character
    Then i try again in another way

  Scenario: Succesful Solution
    Given the link to the challenge
    And a description
    And a set of indications 
    And the indications states a crypt function f(x)=21x+11[26]
    And it says i have to encrypt a word with it
    And i code a script to help me with this task
    And i code another function to convert the characters into numbers
    And i use the alphabet with each position meaning an index for this task
    And i have to decrypt the output of the ciphertext created before
    And the indications states a decrypt function g(y)=ay+b[26]
    And i code a function for this task
    And by this i might find the decryption function
    And by knowing that y=f(x)
    And by kwoning that f(x) stands for the ciphertext
    And i code a function to find the decryption values a, b
    And i use a function to convert numbers into characters
    And i see that the decrypted text is equal to the plain text already given
    And i code another function to decrypt the one that it has to be decrypted
    And i use it with the values a,b already found
    Then i see its output is readable
    And i format the answer 
    And i input the answer 
    And i solve the challenge
