Feature: Solve see through challenge
  from site rankk
  logged in as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS

Scenario: Challenge solved
  Given The current challenge
  When I look at the background image
  And I found a secret code
  Then I solve the challenge
