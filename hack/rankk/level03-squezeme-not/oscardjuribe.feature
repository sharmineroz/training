## Version 2.0
## language: en

Feature: level03-squezeme-not
  Site:
    https://www.rankk.org/challenges/squeeze-me-not.py
  User:
    alestorm980 (wechall)
  Goal:
    Get the flag from the hex message

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04       |
  Machine information:
    Given the challenge description
    And some hex values

  Scenario: Fail: Convert hex to string
    Given some hex values
    When I try to decode using a hex to text convertor
    Then It returns trash characters
    And It doesn't contain the password

  Scenario: Success: Create a rar file
    Given some hex values
    When I use the command
    """
    $echo -n -e "\\x52\\x61\\x72\\x21\\x1A\\x07\\x00\\xCF\\x90\\x73\\x00\\x00
    \\x0D\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x96\\x3E\\x74\\x20\\x90\\x2B\\x00
    \\x18\\x00\\x00\\x00\\x18\\x00\\x00\\x00\\x02\\x91\\x52\\xFB\\x12\\xEC\\x09
    \\x24\\x37\\x1D\\x30\\x06\\x00\\x20\\x00\\x00\\x00\\x73\\x65\\x63\\x72\\x65
    \\x74\\x00\\xB0\\x76\\xB0\\x10\\x54\\x68\\x65\\x20\\x73\\x6F\\x6C\\x75\\x74
    \\x69\\x6F\\x6E\\x20\\x69\\x73\\x20\\x50\\x74\\x6F\\x6C\\x65\\x6D\\x79\\x2E
    \\xC4\\x3D\\x7B\\x00\\x40\\x07\\x00 >output
    """
    Then I check the type of the file with the "file" command
    """
    $ file output
    output: RAR archive data, v4, os: Win32
    """
    When I open the file with the archive manager
    Then I find an interesting file [evidence] (img1.png)
    And I open and find the password of the challenge
