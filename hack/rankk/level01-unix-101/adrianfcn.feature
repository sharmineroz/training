## Version 2.0
## language: en

Feature: unix-101 - miscellaneous - rankk
  Site:
    https://www.rankk.org/challenges/unix-101.py
  User:
    adrianfcn
  Goal:
    Help "Genius" change the permission of a file

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | buster      |
    | Firefox         | 60.9.0      |
  Machine information:
    Given I have access to "https://www.rankk.org/challenges/unix-101.py"

  Scenario: Success: Changing the permissions of a file
    Given I need help the user "Genius"
    And I know he needs to change the permissions of the "restricted" file to
    """
    Owner: full access
    Group: read access
    Other: no access
    """
    And the numeric representation for that is "740"
    And the command for change permissions is "chmod"
    When I input "chmod 740 restricted" in the textbox
    Then I help "Genius" change the permissions
    And I pass the challenge
