## Version 2.0
## language: en

Feature: gremlin-sqli-lord-of-sqlinjection
  Site:
    lord-of-sqlinjection
  User:
    jpverde
  Goal:
    Modify the main query

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0.1      |
  Machine information:
    Given A webpage
    """
    https://los.rubiya.kr/chall/cobolt_b876ab5595253427d3bc34f1cd8f30db.php
    """
    And The webpage shows the php code with the query function

  Scenario: Fail:sql-injection
    Given I am looking at the code shown
    And I can see the function validates if the id is 'admin' and the
    And Password is some md5 encrypted value
    When I type this in the url bypassing the user id as 'admin'
    """
    ?id=admin' or 1=1 --+
    """
    Then I get this response
    """
    Hello rubiya
    You are not admin :(
    """
    And I realize that i have to inject the sql but making the query
    And not to validate Password

    Scenario: Success:sql-injection-modified
    Given I am looking at the response i got last time
    When I type this in the url bypassing the user id as 'admin'
    And commenting the rest of the query so it doesn't validate password
    """
    ?id=admin'--+
    """
    Then I get this response
    """
    COBOLT Clear!
    """
    And I completed the challenge
