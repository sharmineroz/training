Feature:
  Solve Stegano attachment
  From wechall site
  Category Stegano, Image, Training

Background:
  Given I am running Windows 7 (64bit)
  And I am using Google Chrome Version 70.0.3538.77 (Official Build) (64-bit)

Scenario:
  Given the challenge URL
  """
  https://www.wechall.net/challenge/training/stegano/attachment/index.php
  """
  Then I opened that URL with Google Chrome
  And I see the problem statement
  """
  Attachment
  Hello challenger,

  You got mail and a nice attachment.
  Your unique solution which is bound to your session is in there too.

  Enjoy!
  """
  Then I opened the url in the statement
  And I downloaded the picture with extension .php
  And I changed the .php extension by .rar extension
  And I opened the file with winrar.exe version 5.61 (64 bits)
  Then I see the .txt file inside

Scenario: Successful solution
  Given the challenge url
  Then I open it with Google Chrome
  And I opened the .txt file contained in .rar file
  And gave me the follow value
  """
  the answer is RLADNSCPBHMB
  """
  Then I put as answer RLADNSCPBHMB and the answer is correct.
  Then I solved the challenge.
