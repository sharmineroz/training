# Version 2.0
# language: en


Feature:
  Solve Choose your path II
  From wechall site
  Category Linux, Exploit, Warchall

  Background:
    Given I am running Ubuntu 16.04 (64Bit)
    And I am using Mozilla Firefox 63.0

  Scenario: Successful solution
    Given the challenge URL
    """
    https://www.wechall.net/challenge/warchall/choose_your_path2/index.php
    """
    And a SSH access "ssh -p 19198 badwolf10@warchall.net"
    And a source code "charp2.c"
    And a directory
    And I see the challenge statement
    """
    This is the level11 mini challenge found in /home/level/11/ on the
    warchall box. The sequel to Choose your Path.
    You can view the source here.
    """
    Then I log into the remote server with the ssh access
    And I go to the given directory "/home/level/11/"
    And I see the file "charp2.c"
    And I see the solution file "Solution.txt"
    Then I review the given code of "chapr2.c"
    And I notice its function is to count words per line ratio
    And I see they use wc command
    Then I notice I can create a custom wc that the code calls in execution
    Then I create a wc script in my home directory with "vim ~/wc"
    """
    #!/usr/bin/python
    import sys
    fans = open('/home/user/badwolf10/ans.txt','w+')
    fans.write(open(sys.argv[2]).read())
    fans.close()
    """
    Then I add my home directory to the PATH variable
    And I change the permisions of my custom wc file for execution
    Then I run the program at the challenge location "./charp2"
    Then I go to home directory and check the file "ans.txt" was created
    Then I retrieve the content of the file "YourP4thHasBeenCh0sen"
    Then I solve the challenge
