# language: en

Feature: Solve the challenge Just get started
  From the happy-security.de website
  From the JavaScript category
  With my username raballestas

  Background:
    Given an input

  Scenario: Successful solution
    When I look into the source code
    Then I see the submit calls a javascript function get_passwd()
    When I look into the code for the function get_passwd()
    Then I see a var pwd
    Then I write the value of pwd in the input
    And I solve the challenge
