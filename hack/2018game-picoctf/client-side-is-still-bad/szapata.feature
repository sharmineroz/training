## Version 2.0
## language: en

Feature: client-side-is-still-bad  ctf  2018game-picoctf
  Site:
    2018game.picoctf.com
  Category:
    CTF
  User:
    chalimbu
  Goal:
    log in the site

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Google Chrome   | 76.0.3809   |

  Machine information:
    Given I am accessing the web-site through the browser
    When I access the challenge page
    Then I could see the source code of the page

  Scenario: Success: inspect web-page
    Given I am logged in the 2018.picoctf site
    And I access the challenge for Web Exploitation
    When I access the url on the browser
    """
    http://2018shell.picoctf.com:8420
    """
    Then i see the source code of the page
    Given there is some javascript code:
    """
    <script type="text/javascript">
    function verify() {
    checkpass = document.getElementById("pass").value;
    split = 4;
    if (checkpass.substring(split*7, split*8) == '}') {
      if (checkpass.substring(split*6, split*7) == '06ac') {
        if (checkpass.substring(split*5, split*6) == 'd_5e') {
    """
    When the password was by parts as sub-strings
    Then I concatenated the parts starting with picoct
    And continuing to the top with CTF until the } mark
    Then I use that password for login
    And work as expected [evidence](evidence1.png)
