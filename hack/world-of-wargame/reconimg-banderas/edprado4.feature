Feature: Solve Banderas challenge
  from site World of Wargame
  logged as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS

Scenario: Challenge solved
  Given The challenge presented
  When I download the image
  And I crop differents images for each country
  And I search each cropped image in google images
  Then I solve the challenge
